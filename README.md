# Quickstart
Quickstart – WordPress Theme for expert bloggers and magazines.

## Installation

1. Install [Yarn](https://yarnpkg.com/lang/en/docs/install/) on your local machine.
1. Navigate to the theme folder and run `yarn install`.

## Gulp

Available Gulp commands.

##### `gulp` (default)

Will run `gulp copy`, `gulp styles` and `gulp scripts`.

##### `gulp watch`

Will start BrowserSync server and watching changes.

##### `gulp copy`

Will copy vendor JS files into theme folder.

##### `gulp styles`

Will create style.css from `scss/style.scss`.

##### `gulp scripts`

Will concat custom scripts into the single file `js/scripts.scss`.

##### `gulp archive`

Will first run `gulp default` to copy vendor JS files and generate `style.css` and `scripts.js` and then archive all production-ready theme files into one single zip archive.

## Docs

In order to create docs, install [Gitbook](https://toolchain.gitbook.com/setup.html) locally.

Then in the theme folder run `gitbook build . docs`. It will create static documentation in the `/docs` folder. The folder can be specified as source for GitHub Pages.

For development run `gitbook serve` instead.
