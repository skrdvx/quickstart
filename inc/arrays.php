<?php
/**
 * Arrays and utility functions.
 *
 * @package Quickstart
 */

if ( ! function_exists( 'csco_homepage_components_choices' ) ) {
	/**
	 * Returns array of all choices components for home page.
	 */
	function csco_homepage_components_choices() {
		$choices = array(
			'hero'     => esc_html__( 'Hero Section', 'quickstart' ),
			'carousel' => esc_html__( 'Post Carousel', 'quickstart' ),
			'tiles'    => esc_html__( 'Post Tiles', 'quickstart' ),
		);

		return $choices;
	}
}

if ( ! function_exists( 'csco_homepage_components_default' ) ) {
	/**
	 * Returns array of all default components for home page.
	 */
	function csco_homepage_components_default() {
		$default = array( 'hero', 'carousel', 'tiles' );

		return $default;
	}
}
