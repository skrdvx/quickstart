<?php
/**
 * Processing the main query.
 *
 * @package Quickstart
 */

if ( ! function_exists( 'csco_exclude_featured_post_from_homepage_query' ) ) {
	/**
	 * Exclude Featured Post from the Main Query
	 *
	 * @param array $query Default query.
	 * @param bool  $setup Сhange the current query.
	 */
	function csco_exclude_featured_post_from_homepage_query( $query, $setup = true ) {
		if ( is_admin() ) {
			return $query;
		}

		$components = get_theme_mod( 'homepage_components', csco_homepage_components_default() );

		if ( ! is_array( $components ) || ! in_array( 'featured', $components, true ) ) {
			return $query;
		}

		if ( false === get_theme_mod( 'featured_exclude', false ) && true === $setup ) {
			return $query;
		}

		if ( ! ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) || $query->is_home ) ) {
			return $query;
		}

		if ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) && 'page' === get_option( 'show_on_front', 'posts' ) && 'home' === get_theme_mod( 'featured_location', 'front_page' ) ) {
			return $query;
		}

		if ( $query->is_home && 'page' === get_option( 'show_on_front', 'posts' ) && 'front_page' === get_theme_mod( 'featured_location', 'front_page' ) ) {
			return $query;
		}

		if ( ! $query->is_main_query() ) {
			return $query;
		}

		$ids = csco_get_featured_ids();

		if ( ! $ids ) {
			return $query;
		}

		// Return only ids.
		if ( false === $setup ) {
			return $ids;
		}

		$query->set( 'post__not_in', array_merge( $query->get( 'post__not_in' ), $ids ) );

		return $query;
	}
}
add_action( 'pre_get_posts', 'csco_exclude_featured_post_from_homepage_query' );

if ( ! function_exists( 'csco_exclude_tiles_from_homepage_query' ) ) {
	/**
	 * Exclude posts of tiles from the Main Query
	 *
	 * @param array $query Default query.
	 * @param bool  $setup Сhange the current query.
	 */
	function csco_exclude_tiles_from_homepage_query( $query, $setup = true ) {
		if ( is_admin() ) {
			return $query;
		}

		$components = get_theme_mod( 'homepage_components', csco_homepage_components_default() );

		if ( ! is_array( $components ) || ! in_array( 'tiles', $components, true ) ) {
			return $query;
		}

		if ( false === get_theme_mod( 'tiles_exclude', false ) && true === $setup ) {
			return $query;
		}

		if ( ! ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) || $query->is_home ) ) {
			return $query;
		}

		if ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) && 'page' === get_option( 'show_on_front', 'posts' ) && 'home' === get_theme_mod( 'tiles_location', 'front_page' ) ) {
			return $query;
		}

		if ( $query->is_home && 'page' === get_option( 'show_on_front', 'posts' ) && 'front_page' === get_theme_mod( 'tiles_location', 'front_page' ) ) {
			return $query;
		}

		if ( ! $query->is_main_query() ) {
			return $query;
		}

		$ids = csco_get_tiles_ids();

		if ( ! $ids ) {
			return $query;
		}

		// Return only ids.
		if ( false === $setup ) {
			return $ids;
		}

		$query->set( 'post__not_in', array_merge( $query->get( 'post__not_in' ), $ids ) );

		return $query;
	}
}
add_action( 'pre_get_posts', 'csco_exclude_tiles_from_homepage_query' );

if ( ! function_exists( 'csco_exclude_carousel_from_homepage_query' ) ) {
	/**
	 * Exclude posts of carousel from the Main Query
	 *
	 * @param array $query Default query.
	 * @param bool  $setup Сhange the current query.
	 */
	function csco_exclude_carousel_from_homepage_query( $query, $setup = true ) {
		if ( is_admin() ) {
			return $query;
		}

		$components = get_theme_mod( 'homepage_components', csco_homepage_components_default() );

		if ( ! is_array( $components ) || ! in_array( 'carousel', $components, true ) ) {
			return $query;
		}

		if ( false === get_theme_mod( 'carousel_exclude', false ) && true === $setup ) {
			return $query;
		}

		if ( ! ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) || $query->is_home ) ) {
			return $query;
		}

		if ( $query->get( 'page_id' ) === get_option( 'page_on_front' ) && 'page' === get_option( 'show_on_front', 'posts' ) && 'home' === get_theme_mod( 'carousel_location', 'front_page' ) ) {
			return $query;
		}

		if ( $query->is_home && 'page' === get_option( 'show_on_front', 'posts' ) && 'front_page' === get_theme_mod( 'carousel_location', 'front_page' ) ) {
			return $query;
		}

		if ( ! $query->is_main_query() ) {
			return $query;
		}

		$ids = csco_get_carousel_ids();

		if ( ! $ids ) {
			return $query;
		}

		// Return only ids.
		if ( false === $setup ) {
			return $ids;
		}

		$query->set( 'post__not_in', array_merge( $query->get( 'post__not_in' ), $ids ) );

		return $query;
	}
}
add_action( 'pre_get_posts', 'csco_exclude_carousel_from_homepage_query' );
