<?php
/**
 * Header Settings
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'header', array(
		'title'    => esc_html__( 'Header Settings', 'quickstart' ),
		'priority' => 40,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'header_collapsible_general',
		'label'    => esc_html__( 'General', 'quickstart' ),
		'section'  => 'header',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'header_layout',
		'label'    => esc_html__( 'Layout', 'quickstart' ),
		'section'  => 'header',
		'default'  => 'compact',
		'priority' => 10,
		'choices'  => array(
			'compact'      => esc_html__( 'Compact', 'quickstart' ),
			'with-top-bar' => esc_html__( 'With top bar', 'quickstart' ),
			'large'        => esc_html__( 'Large', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'header_top_content',
		'label'           => esc_html__( 'Display additional menu', 'quickstart' ),
		'description'     => esc_html__( 'To display menu you need to assign it to Additional location.', 'quickstart' ),
		'section'         => 'header',
		'default'         => true,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => '==',
				'value'    => 'with-top-bar',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'header_shadow_submenus',
		'label'    => esc_html__( 'Display shadow on submenus', 'quickstart' ),
		'section'  => 'header',
		'default'  => false,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'header_navigation_menu',
		'label'    => esc_html__( 'Display navigation menu', 'quickstart' ),
		'section'  => 'header',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'header_tagline',
		'label'           => esc_html__( 'Display tagline', 'quickstart' ),
		'section'         => 'header',
		'default'         => false,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => '==',
				'value'    => 'large',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'dimension',
		'settings'        => 'header_height',
		'label'           => esc_html__( 'Header Height', 'quickstart' ),
		'section'         => 'header',
		'default'         => 'auto',
		'priority'        => 10,
		'output'          => array(
			array(
				'element'  => '.navbar-topbar .navbar-wrap',
				'property' => 'min-height',
			),
		),
		'active_callback' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => '==',
				'value'    => 'large',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'dimension',
		'settings' => 'header_nav_height',
		'label'    => esc_html__( 'Navigation Bar Height', 'quickstart' ),
		'section'  => 'header',
		'default'  => '80px',
		'priority' => 10,
		'output'   => array(
			array(
				'element'  => '.navbar-primary .navbar-wrap, .navbar-primary .navbar-content',
				'property' => 'height',
			),
			array(
				'element'       => '.offcanvas-header',
				'property'      => 'flex',
				'value_pattern' => '0 0 $',
			),
			array(
				'element'       => '.post-sidebar-shares',
				'property'      => 'top',
				'value_pattern' => 'calc( $ + 20px )',
			),
			array(
				'element'       => '.admin-bar .post-sidebar-shares',
				'property'      => 'top',
				'value_pattern' => 'calc( $ + 52px )',
			),
			array(
				'element'       => '.header-large .post-sidebar-shares',
				'property'      => 'top',
				'value_pattern' => 'calc( $ * 2 + 52px )',
			),
			array(
				'element'       => '.header-large.admin-bar .post-sidebar-shares',
				'property'      => 'top',
				'value_pattern' => 'calc( $ * 2 + 52px )',
			),
		),
	)
);


CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'checkbox',
		'settings'    => 'navbar_sticky',
		'label'       => esc_html__( 'Make navigation bar sticky', 'quickstart' ),
		'description' => esc_html__( 'Enabling this option will make navigation bar visible when scrolling.', 'quickstart' ),
		'section'     => 'header',
		'default'     => true,
		'priority'    => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'effects_navbar_scroll',
		'label'           => esc_html__( 'Enable the smart sticky feature', 'quickstart' ),
		'description'     => esc_html__( 'Enabling this option will reveal navigation bar when scrolling up and hide it when scrolling down.', 'quickstart' ),
		'section'         => 'header',
		'default'         => true,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'navbar_sticky',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'header_offcanvas',
		'label'    => esc_html__( 'Display offcanvas toggle button', 'quickstart' ),
		'section'  => 'header',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'header_navbar_widgets',
		'label'    => esc_html__( 'Display navigation widgets', 'quickstart' ),
		'section'  => 'header',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'header_search_button',
		'label'    => esc_html__( 'Display search button', 'quickstart' ),
		'section'  => 'header',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'header_mega_menu_layout',
		'label'    => esc_html__( 'Mega Menu Subcategories Layout', 'quickstart' ),
		'section'  => 'header',
		'default'  => 'horizontal',
		'priority' => 10,
		'choices'  => array(
			'vertical'   => esc_html__( 'Vertical list', 'quickstart' ),
			'horizontal' => esc_html__( 'Horizontal list', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'header_collapsible_button',
		'label'    => esc_html__( 'Button Link', 'quickstart' ),
		'section'  => 'header',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'text',
		'settings'          => 'header_follow_button_label',
		'label'             => esc_html__( 'Button Label', 'quickstart' ),
		'section'           => 'header',
		'default'           => '' . esc_html__( 'Subscribe', 'quickstart' ),
		'priority'          => 10,
		'sanitize_callback' => 'wp_kses_post',
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'text',
		'settings' => 'header_follow_button_link',
		'label'    => esc_html__( 'Button Link', 'quickstart' ),
		'section'  => 'header',
		'default'  => '',
		'priority' => 10,
	)
);

if ( csco_powerkit_module_enabled( 'social_links' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'collapsible',
			'settings' => 'header_collapsible_social',
			'label'    => esc_html__( 'Social Links', 'quickstart' ),
			'section'  => 'header',
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'checkbox',
			'settings' => 'header_social_links',
			'label'    => esc_html__( 'Display social links', 'quickstart' ),
			'section'  => 'header',
			'default'  => true,
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'select',
			'settings'        => 'header_social_links_scheme',
			'label'           => esc_html__( 'Color scheme', 'quickstart' ),
			'section'         => 'header',
			'default'         => 'light',
			'priority'        => 10,
			'choices'         => array(
				'light'         => esc_html__( 'Light', 'quickstart' ),
				'bold'          => esc_html__( 'Bold', 'quickstart' ),
				'light-rounded' => esc_html__( 'Light Rounded', 'quickstart' ),
				'bold-rounded'  => esc_html__( 'Bold Rounded', 'quickstart' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'header_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'number',
			'settings'        => 'header_social_links_maximum',
			'label'           => esc_html__( 'Maximum Number of Social Links', 'quickstart' ),
			'section'         => 'header',
			'default'         => 3,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'header_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'header_social_links_counts',
			'label'           => esc_html__( 'Display social counts', 'quickstart' ),
			'section'         => 'header',
			'default'         => true,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'header_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);
}
