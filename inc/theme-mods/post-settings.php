<?php
/**
 * Post Settings
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'post_settings', array(
		'title'    => esc_html__( 'Post Settings', 'quickstart' ),
		'priority' => 50,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'post_collapsible_general',
		'label'    => esc_html__( 'General', 'quickstart' ),
		'section'  => 'post_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'post_sidebar',
		'label'    => esc_html__( 'Default Sidebar', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => 'right',
		'priority' => 10,
		'choices'  => array(
			'right'    => esc_html__( 'Right Sidebar', 'quickstart' ),
			'left'     => esc_html__( 'Left Sidebar', 'quickstart' ),
			'disabled' => esc_html__( 'No Sidebar', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'post_meta',
		'label'    => esc_html__( 'Post Meta', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => array( 'category', 'date', 'author', 'shares', 'views', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters( 'csco_post_meta_choices', array(
			'category'     => esc_html__( 'Category', 'quickstart' ),
			'date'         => esc_html__( 'Date', 'quickstart' ),
			'author'       => esc_html__( 'Author', 'quickstart' ),
			'shares'       => esc_html__( 'Shares', 'quickstart' ),
			'views'        => esc_html__( 'Views', 'quickstart' ),
			'comments'     => esc_html__( 'Comments', 'quickstart' ),
			'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'post_header_type',
		'label'    => esc_html__( 'Default Page Header Type', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => 'standard',
		'priority' => 10,
		'choices'  => array(
			'standard' => esc_html__( 'Standard', 'quickstart' ),
			'small'    => esc_html__( 'Small', 'quickstart' ),
			'large'    => esc_html__( 'Large', 'quickstart' ),
			'title'    => esc_html__( 'Page Title Only', 'quickstart' ),
			'none'     => esc_html__( 'None', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'post_media_preview',
		'label'           => esc_html__( 'Standard Page Header Preview', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => 'cropped',
		'priority'        => 10,
		'choices'         => array(
			'cropped'   => esc_html__( 'Display Cropped Image', 'quickstart' ),
			'uncropped' => esc_html__( 'Display Preview in Original Ratio', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'post_header_type',
				'operator' => '==',
				'value'    => 'standard',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'post_prev_next',
		'label'    => esc_html__( 'Prev Next Links', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => 'along',
		'priority' => 10,
		'choices'  => array(
			'disabled' => esc_html__( 'Disabled', 'quickstart' ),
			'along'    => esc_html__( 'Along the edges', 'quickstart' ),
			'below'    => esc_html__( 'Below post content', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'post_tags',
		'label'    => esc_html__( 'Display tags', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'post_excerpt',
		'label'    => esc_html__( 'Display excerpts', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'post_comments_simple',
		'label'    => esc_html__( 'Display comments without the View Comments button', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => false,
		'priority' => 10,
	)
);

if ( csco_powerkit_module_enabled( 'opt_in_forms' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'collapsible',
			'settings' => 'post_collapsible_subscribe',
			'label'    => esc_html__( 'Subscription Form', 'quickstart' ),
			'section'  => 'post_settings',
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'checkbox',
			'settings' => 'post_subscribe',
			'label'    => esc_html__( 'Display subscribe section', 'quickstart' ),
			'section'  => 'post_settings',
			'default'  => false,
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'post_subscribe_name',
			'label'           => esc_html__( 'Display first name field', 'quickstart' ),
			'section'         => 'post_settings',
			'default'         => false,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'post_subscribe',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'              => 'text',
			'settings'          => 'post_subscribe_title',
			'label'             => esc_html__( 'Title', 'quickstart' ),
			'section'           => 'post_settings',
			'default'           => esc_html__( 'Subscribe to Get Our', 'quickstart' ) . '<br> ' . esc_html__( 'Newsletter', 'quickstart' ),
			'priority'          => 10,
			'sanitize_callback' => 'wp_kses_post',
			'active_callback'   => array(
				array(
					'setting'  => 'post_subscribe',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'              => 'text',
			'settings'          => 'post_subscribe_text',
			'label'             => esc_html__( 'Text', 'quickstart' ),
			'section'           => 'post_settings',
			'default'           => esc_html__( 'Get notified of the best deals on our WordPress themes.', 'quickstart' ),
			'priority'          => 10,
			'sanitize_callback' => 'wp_kses_post',
			'active_callback'   => array(
				array(
					'setting'  => 'post_subscribe',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);
}

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'post_collapsible_related',
		'label'    => esc_html__( 'Related Post', 'quickstart' ),
		'section'  => 'post_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'related',
		'label'    => esc_html__( 'Display related section', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'related_layout',
		'label'           => esc_html__( 'Related Post Layout', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => 'grid',
		'priority'        => 10,
		'choices'         => array(
			'list' => esc_html__( 'List', 'quickstart' ),
			'grid' => esc_html__( 'Grid', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'number',
		'settings'        => 'related_number',
		'label'           => esc_html__( 'Maximum Number of Related Posts', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => 4,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'multicheck',
		'settings'        => 'related_post_meta',
		'label'           => esc_attr__( 'Post Meta', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority'        => 10,
		'choices'         => apply_filters( 'csco_post_meta_choices', array(
			'category'     => esc_html__( 'Category', 'quickstart' ),
			'author'       => esc_html__( 'Author', 'quickstart' ),
			'date'         => esc_html__( 'Date', 'quickstart' ),
			'shares'       => esc_html__( 'Shares', 'quickstart' ),
			'views'        => esc_html__( 'Views', 'quickstart' ),
			'comments'     => esc_html__( 'Comments', 'quickstart' ),
			'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
		) ),
		'active_callback' => array(
			array(
				'setting'  => 'related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'related_read_more',
		'label'           => esc_html__( 'Display read more button', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => true,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'text',
		'settings'        => 'related_time_frame',
		'label'           => esc_html__( 'Time Frame', 'quickstart' ),
		'description'     => esc_html__( 'Add period of posts in English. For example: &laquo;2 months&raquo;, &laquo;14 days&raquo; or even &laquo;1 year&raquo;', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => '',
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'related',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'post_collapsible_load_nextpost',
		'label'    => esc_html__( 'Auto Load Next Post', 'quickstart' ),
		'section'  => 'post_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'post_load_nextpost',
		'label'    => esc_html__( 'Enable the Auto Load Next Post feature', 'quickstart' ),
		'section'  => 'post_settings',
		'default'  => false,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'post_load_nextpost_same_category',
		'label'           => esc_html__( 'Auto load posts from the same category only', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => false,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'post_load_nextpost',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'checkbox',
		'settings'        => 'post_load_nextpost_reverse',
		'label'           => esc_html__( 'Auto load previous posts instead of next ones', 'quickstart' ),
		'section'         => 'post_settings',
		'default'         => false,
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'post_load_nextpost',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);
