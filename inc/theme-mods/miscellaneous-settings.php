<?php
/**
 * Miscellaneous Settings
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'miscellaneous', array(
		'title'    => esc_html__( 'Miscellaneous Settings', 'quickstart' ),
		'priority' => 60,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'misc_published_date',
		'label'    => esc_html__( 'Display published date instead of modified date', 'quickstart' ),
		'section'  => 'miscellaneous',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'text',
		'settings' => 'misc_search_placeholder',
		'label'    => esc_html__( 'Search Form Placeholder', 'quickstart' ),
		'section'  => 'miscellaneous',
		'default'  => esc_html__( 'Enter keyword', 'quickstart' ),
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'text',
		'settings' => 'misc_label_readmore',
		'label'    => esc_html__( '"Read More" Button Label', 'quickstart' ),
		'section'  => 'miscellaneous',
		'default'  => esc_html__( 'View Post', 'quickstart' ),
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'misc_classic_gallery_alignment',
		'label'    => esc_html__( 'Alignment of Galleries in Classic Block', 'quickstart' ),
		'section'  => 'miscellaneous',
		'default'  => 'default',
		'priority' => 10,
		'choices'  => array(
			'default' => esc_html__( 'Default', 'quickstart' ),
			'wide'    => esc_html__( 'Wide', 'quickstart' ),
			'large'   => esc_html__( 'Large', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'misc_sticky_sidebar',
		'label'    => esc_html__( 'Sticky Sidebar', 'quickstart' ),
		'section'  => 'miscellaneous',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'misc_sticky_sidebar_method',
		'label'           => esc_html__( 'Sticky Method', 'quickstart' ),
		'section'         => 'miscellaneous',
		'default'         => 'stick-to-top',
		'priority'        => 10,
		'choices'         => array(
			'stick-to-top'    => esc_html__( 'Sidebar top edge', 'quickstart' ),
			'stick-to-bottom' => esc_html__( 'Sidebar bottom edge', 'quickstart' ),
			'stick-last'      => esc_html__( 'Last widget top edge', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'misc_sticky_sidebar',
				'operator' => '==',
				'value'    => true,
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'radio',
		'settings'    => 'webfonts_load_method',
		'label'       => esc_html__( 'Webfonts Load Method', 'quickstart' ),
		'description' => esc_html__( 'Please', 'quickstart' ) . ' <a href="' . add_query_arg( array( 'action' => 'kirki-reset-cache' ), get_site_url() ) . '" target="_blank">' . esc_html__( 'reset font cache', 'quickstart' ) . '</a> ' . esc_html__( 'after saving.', 'quickstart' ),
		'section'     => 'miscellaneous',
		'default'     => 'async',
		'priority'    => 10,
		'choices'     => array(
			'async' => esc_html__( 'Asynchronous', 'quickstart' ),
			'link'  => esc_html__( 'Render-Blocking', 'quickstart' ),
		),
	)
);
