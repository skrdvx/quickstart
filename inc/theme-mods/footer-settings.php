<?php
/**
 * Footer Settings
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'footer', array(
		'title'    => esc_html__( 'Footer Settings', 'quickstart' ),
		'priority' => 40,
	)
);


CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'collapsible',
		'settings' => 'footer_collapsible_general',
		'label'    => esc_html__( 'General', 'quickstart' ),
		'section'  => 'footer',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'footer_layout',
		'label'    => esc_html__( 'Layout', 'quickstart' ),
		'section'  => 'footer',
		'default'  => 'type-1',
		'priority' => 10,
		'choices'  => array(
			'type-1' => esc_html__( 'Type 1', 'quickstart' ),
			'type-2' => esc_html__( 'Type 2', 'quickstart' ),
			'type-3' => esc_html__( 'Type 3', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'textarea',
		'settings'          => 'footer_text',
		'label'             => esc_html__( 'Footer Text', 'quickstart' ),
		'section'           => 'footer',
		/* translators: %s: Author name. */
		'default'           => sprintf( esc_html__( 'Designed & Developed by %s', 'quickstart' ), '<a href="' . esc_url( csco_get_theme_data( 'AuthorURI' ) ) . '">Code Supply Co.</a>' ),
		'priority'          => 10,
		'sanitize_callback' => 'wp_kses_post',
	)
);

if ( csco_powerkit_module_enabled( 'instagram_integration' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'collapsible',
			'settings' => 'footer_collapsible_instagram',
			'label'    => esc_html__( 'Instagram', 'quickstart' ),
			'section'  => 'footer',
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'text',
			'settings' => 'footer_instagram_username',
			'label'    => esc_html__( 'Instagram Username', 'quickstart' ),
			'section'  => 'footer',
			'default'  => '',
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'footer_instagram_header',
			'label'           => esc_html__( 'Display header', 'quickstart' ),
			'section'         => 'footer',
			'default'         => true,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'footer_instagram_username',
					'operator' => '!=',
					'value'    => '',
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'              => 'text',
			'settings'          => 'footer_instagram_title',
			'label'             => esc_html__( 'Title', 'quickstart' ),
			'section'           => 'footer',
			'default'           => esc_html__( 'Our Latest', 'quickstart' ) . '<br> ' . esc_html__( 'Instagram Posts', 'quickstart' ),
			'priority'          => 10,
			'sanitize_callback' => 'wp_kses_post',
			'active_callback'   => array(
				array(
					'setting'  => 'footer_instagram_username',
					'operator' => '!=',
					'value'    => '',
				),
				array(
					'setting'  => 'footer_instagram_header',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'footer_instagram_button',
			'label'           => esc_html__( 'Display follow button', 'quickstart' ),
			'section'         => 'footer',
			'default'         => true,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'footer_instagram_username',
					'operator' => '!=',
					'value'    => '',
				),
			),
		)
	);
}

if ( csco_powerkit_module_enabled( 'social_links' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'collapsible',
			'settings' => 'footer_collapsible_social',
			'label'    => esc_html__( 'Social Links', 'quickstart' ),
			'section'  => 'footer',
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'checkbox',
			'settings' => 'footer_social_links',
			'label'    => esc_html__( 'Display social links', 'quickstart' ),
			'section'  => 'footer',
			'default'  => false,
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'radio',
			'settings' => 'footer_social_links_direction',
			'label'    => esc_html__( 'Direction', 'quickstart' ),
			'section'  => 'footer',
			'default'  => 'vertical',
			'priority' => 10,
			'choices'  => array(
				'vertical'      => esc_html__( 'Vertical', 'quickstart' ),
				'horizontal' => esc_html__( 'Horizontal', 'quickstart' ),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'select',
			'settings'        => 'footer_social_links_scheme',
			'label'           => esc_html__( 'Color scheme', 'quickstart' ),
			'section'         => 'footer',
			'default'         => 'light',
			'priority'        => 10,
			'choices'         => array(
				'light'         => esc_html__( 'Light', 'quickstart' ),
				'bold'          => esc_html__( 'Bold', 'quickstart' ),
				'light-rounded' => esc_html__( 'Light Rounded', 'quickstart' ),
				'bold-rounded'  => esc_html__( 'Bold Rounded', 'quickstart' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'footer_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'number',
			'settings'        => 'footer_social_links_maximum',
			'label'           => esc_html__( 'Maximum Number of Social Links', 'quickstart' ),
			'section'         => 'footer',
			'default'         => 3,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'footer_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'footer_social_links_counts',
			'label'           => esc_html__( 'Display counts', 'quickstart' ),
			'section'         => 'footer',
			'default'         => true,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'footer_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);
}
