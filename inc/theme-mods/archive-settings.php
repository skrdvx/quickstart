<?php
/**
 * Archive Settings
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'archive_settings', array(
		'title'    => esc_html__( 'Archive Settings', 'quickstart' ),
		'priority' => 50,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'archive_layout',
		'label'    => esc_html__( 'Layout', 'quickstart' ),
		'section'  => 'archive_settings',
		'default'  => 'masonry',
		'priority' => 10,
		'choices'  => array(
			'full'    => esc_html__( 'Full Post Layout', 'quickstart' ),
			'list'    => esc_html__( 'List Layout', 'quickstart' ),
			'grid'    => esc_html__( 'Grid Layout', 'quickstart' ),
			'masonry' => esc_html__( 'Masonry Layout', 'quickstart' ),
			'mixed'   => esc_html__( 'Mixed Layout', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'archive_sidebar',
		'label'    => esc_html__( 'Sidebar', 'quickstart' ),
		'section'  => 'archive_settings',
		'default'  => 'right',
		'priority' => 10,
		'choices'  => array(
			'right'    => esc_html__( 'Right Sidebar', 'quickstart' ),
			'left'     => esc_html__( 'Left Sidebar', 'quickstart' ),
			'disabled' => esc_html__( 'No Sidebar', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'archive_post_meta',
		'label'    => esc_html__( 'Post Meta', 'quickstart' ),
		'section'  => 'archive_settings',
		'default'  => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters( 'csco_post_meta_choices', array(
			'category'     => esc_html__( 'Category', 'quickstart' ),
			'author'       => esc_html__( 'Author', 'quickstart' ),
			'date'         => esc_html__( 'Date', 'quickstart' ),
			'shares'       => esc_html__( 'Shares', 'quickstart' ),
			'views'        => esc_html__( 'Views', 'quickstart' ),
			'comments'     => esc_html__( 'Comments', 'quickstart' ),
			'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'archive_media_preview',
		'label'           => esc_html__( 'Post Preview Image Size', 'quickstart' ),
		'section'         => 'archive_settings',
		'default'         => 'cropped',
		'priority'        => 10,
		'choices'         => array(
			'cropped'   => esc_html__( 'Display Cropped Image', 'quickstart' ),
			'uncropped' => esc_html__( 'Display Preview in Original Ratio', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				array(
					'setting'  => 'archive_layout',
					'operator' => '==',
					'value'    => 'full',
				),
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'archive_summary',
		'label'           => esc_html__( 'Full Post Summary', 'quickstart' ),
		'section'         => 'archive_settings',
		'default'         => 'excerpt',
		'priority'        => 10,
		'choices'         => array(
			'excerpt' => esc_html__( 'Use Excerpts', 'quickstart' ),
			'content' => esc_html__( 'Use Read More Tag', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				array(
					'setting'  => 'archive_layout',
					'operator' => '==',
					'value'    => 'full',
				),
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'archive_read_more',
		'label'    => esc_html__( 'Display read more button', 'quickstart' ),
		'section'  => 'archive_settings',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'archive_pagination_type',
		'label'    => esc_html__( 'Pagination', 'quickstart' ),
		'section'  => 'archive_settings',
		'default'  => 'load-more',
		'priority' => 10,
		'choices'  => array(
			'standard'  => esc_html__( 'Standard', 'quickstart' ),
			'load-more' => esc_html__( 'Load More Button', 'quickstart' ),
			'infinite'  => esc_html__( 'Infinite Load', 'quickstart' ),
		),
	)
);
