<?php
/**
 * Typography
 *
 * @package Quickstart
 */

CSCO_Kirki::add_panel(
	'typography', array(
		'title'    => esc_html__( 'Typography', 'quickstart' ),
		'priority' => 30,
	)
);

CSCO_Kirki::add_section(
	'typography_general', array(
		'title'    => esc_html__( 'General', 'quickstart' ),
		'panel'    => 'typography',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'typography',
		'settings' => 'font_base',
		'label'    => esc_html__( 'Base Font', 'quickstart' ),
		'section'  => 'typography_general',
		'default'  => array(
			'font-family'    => 'inter',
			'variant'        => 'regular',
			'subsets'        => array( 'latin' ),
			'font-size'      => '1rem',
			'letter-spacing' => '0',
		),
		'choices'  => apply_filters( 'powerkit_fonts_choices', array(
			'variant' => array(
				'regular',
				'italic',
				'600',
				'700',
				'700italic',
				'800',
			),
		) ),
		'priority' => 10,
		'output'   => apply_filters( 'csco_font_base', array(
			array(
				'element' => 'body',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_primary',
		'label'       => esc_html__( 'Primary Font', 'quickstart' ),
		'description' => esc_html__( 'Used for buttons, categories and tags, post meta links and other actionable elements.', 'quickstart' ),
		'section'     => 'typography_general',
		'default'     => array(
			'font-family'    => 'inter',
			'variant'        => '700',
			'subsets'        => array( 'latin' ),
			'font-size'      => '0.625rem',
			'letter-spacing' => '0',
			'text-transform' => 'uppercase',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array(
			'variant' => array(
				'regular',
				'600',
				'700',
			),
		) ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_primary', array(
			array(
				'element' => 'button, .button, input[type="button"], input[type="reset"], input[type="submit"], .cs-font-primary, .no-comments, .text-action, .archive-wrap .more-link, .share-total, .nav-links, .comment-reply-link, .post-sidebar-tags a, .read-more, .entry-more a, .navigation.pagination .nav-links > span, .navigation.pagination .nav-links > a, .subcategories .cs-nav-link, .cs-social-accounts .cs-social-label, .author-social-accounts .author-social-label',
			),
			array(
				'element' => '.pk-font-primary, .entry-meta-details .pk-share-buttons-count, .entry-meta-details .pk-share-buttons-label, .post-sidebar-shares .pk-share-buttons-label, .footer-instagram .instagram-username, .pk-twitter-counters .number, .pk-instagram-counters, .pk-instagram-counters .number, .pk-alt-instagram-counters .number, .pk-share-buttons-after-post .pk-share-buttons-total',
			),
			array(
				'element' => '.widget_rss ul li .rss-date, .widget_rss ul li cite, .widget_tag_cloud .tagcloud a, caption',
			),
			array(
				'element' => '.widget_categories li, .widget_archive li',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_secondary',
		'label'       => esc_html__( 'Secondary Font', 'quickstart' ),
		'description' => esc_html__( 'Used for post meta, image captions and other secondary elements.', 'quickstart' ),
		'section'     => 'typography_general',
		'default'     => array(
			'font-family'    => 'inter',
			'subsets'        => array( 'latin' ),
			'variant'        => '400',
			'font-size'      => '0.625rem',
			'letter-spacing' => '0',
			'text-transform' => 'uppercase',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array(
			'variant' => array(
				'regular',
				'600',
				'700',
			),
		) ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_secondary', array(
			array(
				'element' => 'small, input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], input[type="number"], input[type="tel"], input[type="range"], input[type="date"], input[type="month"], input[type="week"], input[type="time"], input[type="datetime"], input[type="datetime-local"], input[type="color"], div[class*="meta-"], span[class*="meta-"], select, textarea, label, .cs-font-secondary, .post-meta, .entry-read-more, .archive-count, .page-subtitle, .site-description, figcaption, .post-tags a, .tagcloud a, .post-prev-next .link-label a, .post-format-icon, .comment-metadata, .says, .logged-in-as, .must-log-in, .navbar-brand .tagline, .post-sidebar-shares .total-shares, .cs-breadcrumbs, .searchwp-live-search-no-results em, .searchwp-live-search-no-min-chars:after, .cs-video-tools-large .cs-tooltip, .entry-details .author-wrap a, .footer-copyright',
			),
			array(
				'element' => '.wp-caption-text, .wp-block-image figcaption, .wp-block-audio figcaption, .wp-block-embed figcaption, .wp-block-pullquote cite, .wp-block-pullquote footer, .wp-block-pullquote .wp-block-pullquote__citation, blockquote cite, .wp-block-quote cite',
			),
			array(
				'element' => '.pk-font-secondary, .pk-alt-instagram-counters, .pk-twitter-counters, .pk-instagram-item .pk-instagram-data .pk-meta, .pk-alt-instagram-item .pk-alt-instagram-data .pk-meta, .entry-share .pk-share-buttons-total',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'typography',
		'settings' => 'font_entry_excerpt',
		'label'    => esc_html__( 'Entry Excerpt', 'quickstart' ),
		'section'  => 'typography_general',
		'default'  => array(
			'font-size'   => '0.875rem',
			'line-height' => '1.5',
		),
		'choices'  => apply_filters( 'powerkit_fonts_choices', array(
			'variant' => array(
				'regular',
				'italic',
				'600',
				'700',
				'700italic',
				'800',
			),
		) ),
		'priority' => 10,
		'output'   => apply_filters( 'csco_font_entry_excerpt', array(
			array(
				'element' => '.entry-excerpt, .post-excerpt, .pk-subscribe-form-wrap .pk-privacy label',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod',
	array(
		'type'     => 'typography',
		'settings' => 'font_post_content',
		'label'    => esc_html__( 'Post Content', 'quickstart' ),
		'section'  => 'typography_general',
		'default'  => array(
			'font-family'    => 'inherit',
			'variant'        => 'inherit',
			'subsets'        => array( 'latin' ),
			'font-size'      => '1rem',
			'letter-spacing' => 'inherit',
		),
		'choices'  => apply_filters( 'powerkit_fonts_choices', array(
			'variant' => array(
				'regular',
				'italic',
				'600',
				'700',
				'700italic',
				'800',
			),
		) ),
		'priority' => 10,
		'output'   => apply_filters( 'csco_font_post_content', array(
			array(
				'element' => '.entry-content',
			),
		) ),
	)
);

CSCO_Kirki::add_section(
	'typography_logos', array(
		'title'    => esc_html__( 'Logos', 'quickstart' ),
		'panel'    => 'typography',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_main_logo',
		'label'       => esc_html__( 'Main Logo', 'quickstart' ),
		'description' => esc_html__( 'The main logo is used in the navigation bar and mobile view of your website.', 'quickstart' ),
		'section'     => 'typography_logos',
		'default'     => array(
			'font-family'    => 'inter',
			'font-size'      => '2.125rem',
			'variant'        => '700',
			'subsets'        => array( 'latin' ),
			'letter-spacing' => '-0.065em',
			'text-transform' => 'capitalize',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_logo', array(
			array(
				'element' => '.site-title',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'typography',
		'settings'        => 'font_large_logo',
		'label'           => esc_html__( 'Large Logo', 'quickstart' ),
		'section'         => 'typography_logos',
		'default'         => array(
			'font-family'    => 'inter',
			'font-size'      => '2.25rem',
			'variant'        => '700',
			'subsets'        => array( 'latin' ),
			'letter-spacing' => '-0.065em',
			'text-transform' => 'capitalize',
		),
		'description'     => esc_html__( 'The large logo is used in the site header in desktop view.', 'quickstart' ),
		'choices'         => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'        => 10,
		'active_callback' => array(
			array(
				'setting'  => 'header_layout',
				'operator' => '==',
				'value'    => 'large',
			),
		),
		'output'          => apply_filters( 'csco_font_large_logo', array(
			array(
				'element' => '.large-title',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_footer_logo',
		'label'       => esc_html__( 'Footer Logo', 'quickstart' ),
		'description' => esc_html__( 'The footer logo is used in the site footer in desktop and mobile view.', 'quickstart' ),
		'section'     => 'typography_logos',
		'default'     => array(
			'font-family'    => 'inter',
			'font-size'      => '2.125rem',
			'variant'        => '700',
			'subsets'        => array( 'latin' ),
			'letter-spacing' => '-0.075em',
			'text-transform' => '',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_footer_logo', array(
			array(
				'element' => '.footer-title',
			),
		) ),
	)
);

CSCO_Kirki::add_section(
	'typography_headings', array(
		'title'    => esc_html__( 'Headings', 'quickstart' ),
		'panel'    => 'typography',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'typography',
		'settings' => 'font_headings',
		'label'    => esc_html__( 'Headings', 'squaretype' ),
		'section'  => 'typography_headings',
		'default'  => array(
			'font-family'    => 'inter',
			'variant'        => '700',
			'subsets'        => array( 'latin' ),
			'letter-spacing' => '-0.025em',
			'text-transform' => 'none',
		),
		'choices'  => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority' => 10,
		'output'   => apply_filters( 'csco_font_headings', array(
			array(
				'element' => 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .comment-author .fn, blockquote, .pk-font-heading, .post-sidebar-date .reader-text, .wp-block-quote, .wp-block-cover .wp-block-cover-image-text, .wp-block-cover .wp-block-cover-text, .wp-block-cover h2, .wp-block-cover-image .wp-block-cover-image-text, .wp-block-cover-image .wp-block-cover-text, .wp-block-cover-image h2, .wp-block-pullquote p, p.has-drop-cap:not(:focus):first-letter, .pk-font-heading',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod',
	array(
		'type'        => 'typography',
		'settings'    => 'font_title_primary',
		'label'       => esc_html__( 'Section Titles Primary', 'quickstart' ),
		'description' => esc_html__( 'Used for widget, related posts and other sections\' titles.', 'quickstart' ),
		'section'     => 'typography_headings',
		'default'     => array(
			'font-family'    => 'inter',
			'variant'        => '800',
			'subsets'        => array( 'latin' ),
			'font-size'      => '1.625rem',
			'letter-spacing' => '-0.02em',
			'text-transform' => 'none',
			'color'          => '#000000',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_title_block', array(
			array(
				'element' => '.title-block, .pk-font-block',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod',
	array(
		'type'        => 'typography',
		'settings'    => 'font_title_secondary',
		'label'       => esc_html__( 'Section Titles Secondary', 'quickstart' ),
		'description' => esc_html__( 'Used for widget, related posts and other sections\' titles.', 'quickstart' ),
		'section'     => 'typography_headings',
		'default'     => array(
			'font-family'    => 'inter',
			'variant'        => '600',
			'subsets'        => array( 'latin' ),
			'font-size'      => '0.625rem',
			'text-transform' => 'uppercase',
			'color'          => '#ADADAD',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_title_block', array(
			array(
				'element' => '.title-style',
			),
		) ),
	)
);

CSCO_Kirki::add_section(
	'typography_navigation', array(
		'title'    => esc_html__( 'Navigation', 'quickstart' ),
		'panel'    => 'typography',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_menu',
		'label'       => esc_html__( 'Menu Font', 'quickstart' ),
		'description' => esc_html__( 'Used for main top level menu elements.', 'quickstart' ),
		'section'     => 'typography_navigation',
		'default'     => array(
			'font-family'    => 'inter',
			'variant'        => '600',
			'subsets'        => array( 'latin' ),
			'font-size'      => '1rem',
			'letter-spacing' => '0',
			'text-transform' => 'none',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_menu', array(
			array(
				'element' => '.navbar-nav > li > a, .cs-mega-menu-child > a, .widget_archive li a, .widget_categories li a, .widget_meta li a, .widget_nav_menu .menu > li > a, .widget_pages .page_item a',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_submenu',
		'label'       => esc_html__( 'Submenu Font', 'quickstart' ),
		'description' => esc_html__( 'Used for submenu elements.', 'quickstart' ),
		'section'     => 'typography_navigation',
		'default'     => array(
			'font-family'    => 'inter',
			'subsets'        => array( 'latin' ),
			'variant'        => '700',
			'font-size'      => '0.875rem',
			'letter-spacing' => '0',
			'text-transform' => 'none',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_font_submenu', array(
			array(
				'element' => '.navbar-nav .sub-menu > li > a, .widget_categories .children li a, .widget_pages .children li a, .widget_nav_menu .sub-menu > li > a',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'typography',
		'settings'    => 'font_additional_menu',
		'label'       => esc_html__( 'Additional Menu', 'quickstart' ),
		'description' => esc_html__( 'Used for additional menu elements.', 'quickstart' ),
		'section'     => 'typography_navigation',
		'default'     => array(
			'font-family'    => 'inter',
			'variant'        => '400',
			'subsets'        => array( 'latin' ),
			'font-size'      => '0.8125rem',
			'letter-spacing' => '0',
			'text-transform' => 'none',
		),
		'choices'     => apply_filters( 'powerkit_fonts_choices', array() ),
		'priority'    => 10,
		'output'      => apply_filters( 'csco_additional_menu', array(
			array(
				'element' => '#menu-additional.navbar-nav > li > a',
			),
			array(
				'element' => '.navbar-topbar .navbar-nav > li > a',
			),
		) ),
	)
);
