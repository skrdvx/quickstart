<?php
/**
 * Homepage Settings
 *
 * @package Quickstart
 */

/**
 * Removes default WordPress Static Front Page section
 * and re-adds it in our own panel with the same parameters.
 *
 * @param object $wp_customize Instance of the WP_Customize_Manager class.
 */
function csco_reorder_customizer_settings( $wp_customize ) {

	// Get current front page section parameters.
	$static_front_page = $wp_customize->get_section( 'static_front_page' );

	// Remove existing section, so that we can later re-add it to our panel.
	$wp_customize->remove_section( 'static_front_page' );

	// Re-add static front page section with a new name, but same description.
	$wp_customize->add_section( 'static_front_page', array(
		'title'           => esc_html__( 'Static Front Page', 'quickstart' ),
		'priority'        => 20,
		'description'     => $static_front_page->description,
		'panel'           => 'homepage_settings',
		'active_callback' => $static_front_page->active_callback,
	) );
}
add_action( 'customize_register', 'csco_reorder_customizer_settings' );

CSCO_Kirki::add_panel(
	'homepage_settings', array(
		'title'    => esc_html__( 'Homepage Settings', 'quickstart' ),
		'priority' => 50,
	)
);

CSCO_Kirki::add_section(
	'homepage_layout', array(
		'title'    => esc_html__( 'Homepage Layout', 'quickstart' ),
		'priority' => 15,
		'panel'    => 'homepage_settings',
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'home_layout',
		'label'    => esc_html__( 'Layout', 'quickstart' ),
		'section'  => 'homepage_layout',
		'default'  => 'masonry',
		'priority' => 10,
		'choices'  => array(
			'full'    => esc_html__( 'Full Post Layout', 'quickstart' ),
			'list'    => esc_html__( 'List Layout', 'quickstart' ),
			'grid'    => esc_html__( 'Grid Layout', 'quickstart' ),
			'masonry' => esc_html__( 'Masonry Layout', 'quickstart' ),
			'mixed'   => esc_html__( 'Mixed Layout', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'home_sidebar',
		'label'    => esc_html__( 'Sidebar', 'quickstart' ),
		'section'  => 'homepage_layout',
		'default'  => 'right',
		'priority' => 10,
		'choices'  => array(
			'right'    => esc_html__( 'Right Sidebar', 'quickstart' ),
			'left'     => esc_html__( 'Left Sidebar', 'quickstart' ),
			'disabled' => esc_html__( 'No Sidebar', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'home_post_meta',
		'label'    => esc_html__( 'Post Meta', 'quickstart' ),
		'section'  => 'homepage_layout',
		'default'  => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters( 'csco_post_meta_choices', array(
			'category'     => esc_html__( 'Category', 'quickstart' ),
			'author'       => esc_html__( 'Author', 'quickstart' ),
			'date'         => esc_html__( 'Date', 'quickstart' ),
			'shares'       => esc_html__( 'Shares', 'quickstart' ),
			'views'        => esc_html__( 'Views', 'quickstart' ),
			'comments'     => esc_html__( 'Comments', 'quickstart' ),
			'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'home_read_more',
		'label'    => esc_html__( 'Display read more button', 'quickstart' ),
		'section'  => 'homepage_layout',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'home_media_preview',
		'label'           => esc_html__( 'Post Preview Image Size', 'quickstart' ),
		'section'         => 'homepage_layout',
		'default'         => 'cropped',
		'priority'        => 10,
		'choices'         => array(
			'cropped'   => esc_html__( 'Display Cropped Image', 'quickstart' ),
			'uncropped' => esc_html__( 'Display Preview in Original Ratio', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				array(
					'setting'  => 'home_layout',
					'operator' => '==',
					'value'    => 'full',
				),
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'home_summary',
		'label'           => esc_html__( 'Full Post Summary', 'quickstart' ),
		'section'         => 'homepage_layout',
		'default'         => 'excerpt',
		'priority'        => 10,
		'choices'         => array(
			'excerpt' => esc_html__( 'Use Excerpts', 'quickstart' ),
			'content' => esc_html__( 'Use Read More Tag', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'home_layout',
				'operator' => '==',
				'value'    => 'full',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'home_pagination_type',
		'label'    => esc_html__( 'Pagination', 'quickstart' ),
		'section'  => 'homepage_layout',
		'default'  => 'load-more',
		'priority' => 10,
		'choices'  => array(
			'standard'  => esc_html__( 'Standard', 'quickstart' ),
			'load-more' => esc_html__( 'Load More Button', 'quickstart' ),
			'infinite'  => esc_html__( 'Infinite Load', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_section(
	'hero', array(
		'title'    => esc_html__( 'Hero Section', 'quickstart' ),
		'panel'    => 'homepage_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'image',
		'settings' => 'hero_image',
		'label'    => esc_html__( 'Image', 'quickstart' ),
		'section'  => 'hero',
		'default'  => '',
		'priority' => 10,
		'choices'  => array(
			'save_as' => 'id',
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'hero_layout',
		'label'           => esc_html__( 'Layout', 'quickstart' ),
		'section'         => 'hero',
		'default'         => 'left',
		'priority'        => 10,
		'choices'         => array(
			'left'  => esc_html__( 'Left alignment', 'quickstart' ),
			'right' => esc_html__( 'Right alignment', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'hero_image',
				'operator' => '!==',
				'value'    => '',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'text',
		'settings' => 'hero_title',
		'label'    => esc_html__( 'Title', 'quickstart' ),
		'section'  => 'hero',
		'default'  => esc_html__( 'Stay Cool Italian Style', 'quickstart' ),
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'textarea',
		'settings' => 'hero_text',
		'label'    => esc_html__( 'Text', 'quickstart' ),
		'section'  => 'hero',
		'default'  => esc_html__( 'Aenean eleifend ante maecenas pulvinar montes lorem et pede dis dolor pretium donec dictum. Vici consequat justo enim.', 'quickstart' ),
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'hero_subscribe',
		'label'    => esc_html__( 'Display subscription form', 'quickstart' ),
		'section'  => 'hero',
		'default'  => true,
		'priority' => 10,
	)
);

if ( csco_powerkit_module_enabled( 'opt_in_forms' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'hero_subscription_name',
			'label'           => esc_html__( 'Display first name field', 'quickstart' ),
			'section'         => 'hero',
			'default'         => false,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'hero_subscribe',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);
}

if ( csco_powerkit_module_enabled( 'social_links' ) ) {
	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'checkbox',
			'settings' => 'hero_social_links',
			'label'    => esc_html__( 'Display social links', 'quickstart' ),
			'section'  => 'hero',
			'default'  => false,
			'priority' => 10,
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'select',
			'settings'        => 'hero_social_links_scheme',
			'label'           => esc_html__( 'Color scheme', 'quickstart' ),
			'section'         => 'hero',
			'default'         => 'light',
			'priority'        => 10,
			'choices'         => array(
				'light'         => esc_html__( 'Light', 'quickstart' ),
				'bold'          => esc_html__( 'Bold', 'quickstart' ),
				'light-rounded' => esc_html__( 'Light Rounded', 'quickstart' ),
				'bold-rounded'  => esc_html__( 'Bold Rounded', 'quickstart' ),
			),
			'active_callback' => array(
				array(
					'setting'  => 'hero_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'number',
			'settings'        => 'hero_social_links_maximum',
			'label'           => esc_html__( 'Maximum Number of Social Links', 'quickstart' ),
			'section'         => 'hero',
			'default'         => 3,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'hero_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'checkbox',
			'settings'        => 'hero_social_links_counts',
			'label'           => esc_html__( 'Display social links counts', 'quickstart' ),
			'section'         => 'hero',
			'default'         => true,
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'hero_social_links',
					'operator' => '==',
					'value'    => true,
				),
			),
		)
	);
}

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'hero_image_mask',
		'label'           => esc_html__( 'Mask', 'quickstart' ),
		'section'         => 'hero',
		'default'         => 'circle',
		'priority'        => 10,
		'choices'         => array(
			'circle'  => esc_html__( 'Circle', 'quickstart' ),
			'square'  => esc_html__( 'Square', 'quickstart' ),
			'hexagon' => esc_html__( 'Hexagon', 'quickstart' ),
			'none'    => esc_html__( 'None', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'hero_image',
				'operator' => '!==',
				'value'    => '',
			),
		),
	)
);

CSCO_Kirki::add_section(
	'tiles', array(
		'title'    => esc_html__( 'Post Tiles', 'quickstart' ),
		'panel'    => 'homepage_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'radio',
		'settings' => 'tiles_type',
		'label'    => esc_html__( 'Type', 'quickstart' ),
		'section'  => 'tiles',
		'default'  => 'type-1',
		'priority' => 10,
		'choices'  => array(
			'type-1' => esc_html__( 'Type 1', 'quickstart' ),
			'type-2' => esc_html__( 'Type 2', 'quickstart' ),
			'type-3' => esc_html__( 'Type 3', 'quickstart' ),
			'type-4' => esc_html__( 'Type 4', 'quickstart' ),
			'type-5' => esc_html__( 'Type 5', 'quickstart' ),
			'type-6' => esc_html__( 'Type 6', 'quickstart' ),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'text',
		'settings'          => 'tile_1_title',
		'label'             => esc_html__( 'Title', 'quickstart' ),
		'section'           => 'tiles',
		'default'           => esc_html__( '[[Latest]] Trending Posts', 'quickstart' ),
		'priority'          => 10,
		'sanitize_callback' => 'wp_kses_post',
		'active_callback'   => array(
			array(
				'setting'  => 'tiles_type',
				'operator' => '==',
				'value'    => 'type-1',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'tiles_location',
		'label'           => esc_html__( 'Display Location', 'quickstart' ),
		'section'         => 'tiles',
		'default'         => 'front_page',
		'priority'        => 10,
		'choices'         => array(
			'front_page' => esc_html__( 'Homepage', 'quickstart' ),
			'home'       => esc_html__( 'Posts page', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'show_on_front',
				'operator' => '==',
				'value'    => 'page',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'tiles_simple_meta',
		'label'    => esc_attr__( 'Post Meta of Simple Posts', 'quickstart' ),
		'section'  => 'tiles',
		'default'  => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters(
			'csco_post_meta_choices',
			array(
				'category'     => esc_html__( 'Category', 'quickstart' ),
				'author'       => esc_html__( 'Author', 'quickstart' ),
				'date'         => esc_html__( 'Date', 'quickstart' ),
				'shares'       => esc_html__( 'Shares', 'quickstart' ),
				'views'        => esc_html__( 'Views', 'quickstart' ),
				'comments'     => esc_html__( 'Comments', 'quickstart' ),
				'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
			)
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'tiles_full_meta',
		'label'    => esc_attr__( 'Post Meta of Large Posts', 'quickstart' ),
		'section'  => 'tiles',
		'default'  => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters(
			'csco_post_meta_choices',
			array(
				'category'     => esc_html__( 'Category', 'quickstart' ),
				'author'       => esc_html__( 'Author', 'quickstart' ),
				'date'         => esc_html__( 'Date', 'quickstart' ),
				'shares'       => esc_html__( 'Shares', 'quickstart' ),
				'views'        => esc_html__( 'Views', 'quickstart' ),
				'comments'     => esc_html__( 'Comments', 'quickstart' ),
				'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
			)
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'home_tiles_read_more',
		'label'    => esc_html__( 'Display read more button', 'quickstart' ),
		'section'  => 'tiles',
		'default'  => true,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'tiles_filter_categories',
		'label'       => esc_html__( 'Filter by Categories', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of category slugs. For example: &laquo;travel, lifestyle, food&raquo;. Leave empty for all categories.', 'quickstart' ),
		'section'     => 'tiles',
		'default'     => '',
		'priority'    => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'tiles_filter_tags',
		'label'       => esc_html__( 'Filter by Tags', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of tag slugs. For example: &laquo;worth-reading, top-5, playlists&raquo;. Leave empty for all tags.', 'quickstart' ),
		'section'     => 'tiles',
		'default'     => '',
		'priority'    => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'tiles_filter_posts',
		'label'       => esc_html__( 'Filter by Posts', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of post IDs. For example: 12, 34, 145. Leave empty for all posts.', 'quickstart' ),
		'section'     => 'tiles',
		'default'     => '',
		'priority'    => 10,
	)
);

if ( class_exists( 'Post_Views_Counter' ) ) {

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'radio',
			'settings' => 'tiles_orderby',
			'label'    => esc_html__( 'Order posts by', 'quickstart' ),
			'section'  => 'tiles',
			'default'  => 'date',
			'priority' => 10,
			'choices'  => array(
				'date'       => esc_html__( 'Date', 'quickstart' ),
				'post_views' => esc_html__( 'Views', 'quickstart' ),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'text',
			'settings'        => 'tiles_time_frame',
			'label'           => esc_html__( 'Filter by Time Frame', 'quickstart' ),
			'description'     => esc_html__( 'Add period of posts in English. For example: &laquo;2 months&raquo;, &laquo;14 days&raquo; or even &laquo;1 year&raquo;', 'quickstart' ),
			'section'         => 'tiles',
			'default'         => '',
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'tiles_orderby',
					'operator' => '==',
					'value'    => 'post_views',
				),
			),
		)
	);
}

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'tiles_exclude',
		'label'    => esc_html__( 'Exclude posts of tiles from the main archive', 'quickstart' ),
		'section'  => 'tiles',
		'default'  => false,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'checkbox',
		'settings'    => 'tiles_avoid_duplicate',
		'label'       => esc_html__( 'Exclude duplicate posts from the post tiles', 'quickstart' ),
		'description' => esc_html__( 'If enabled, posts from the upper sections will not be shown in the post tiles. The "Filter by Posts" option will override this option.', 'quickstart' ),
		'section'     => 'tiles',
		'default'     => false,
		'priority'    => 10,
	)
);

CSCO_Kirki::add_section(
	'carousel', array(
		'title'    => esc_html__( 'Post Carousel', 'quickstart' ),
		'panel'    => 'homepage_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'radio',
		'settings'        => 'carousel_location',
		'label'           => esc_html__( 'Display Location', 'quickstart' ),
		'section'         => 'carousel',
		'default'         => 'front_page',
		'priority'        => 10,
		'choices'         => array(
			'front_page' => esc_html__( 'Homepage', 'quickstart' ),
			'home'       => esc_html__( 'Posts page', 'quickstart' ),
		),
		'active_callback' => array(
			array(
				'setting'  => 'show_on_front',
				'operator' => '==',
				'value'    => 'page',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'text',
		'settings'          => 'carousel_title',
		'label'             => esc_html__( 'Title', 'quickstart' ),
		'section'           => 'carousel',
		'default'           => esc_html__( '[[Social Links]] Latest Posts', 'quickstart' ),
		'priority'          => 10,
		'sanitize_callback' => 'wp_kses_post',
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'number',
		'settings' => 'carousel_number_slides',
		'label'    => esc_html__( 'Number of Slides', 'quickstart' ),
		'section'  => 'carousel',
		'default'  => 10,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'multicheck',
		'settings' => 'carousel_meta',
		'label'    => esc_attr__( 'Post Meta', 'quickstart' ),
		'section'  => 'carousel',
		'default'  => array( 'category', 'author', 'date', 'shares', 'views', 'comments', 'reading_time' ),
		'priority' => 10,
		'choices'  => apply_filters(
			'csco_post_meta_choices',
			array(
				'category'     => esc_html__( 'Category', 'quickstart' ),
				'author'       => esc_html__( 'Author', 'quickstart' ),
				'date'         => esc_html__( 'Date', 'quickstart' ),
				'shares'       => esc_html__( 'Shares', 'quickstart' ),
				'views'        => esc_html__( 'Views', 'quickstart' ),
				'comments'     => esc_html__( 'Comments', 'quickstart' ),
				'reading_time' => esc_html__( 'Reading Time', 'quickstart' ),
			)
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'carousel_filter_categories',
		'label'       => esc_html__( 'Filter by Categories', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of category slugs. For example: &laquo;travel, lifestyle, food&raquo;. Leave empty for all categories.', 'quickstart' ),
		'section'     => 'carousel',
		'default'     => '',
		'priority'    => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'carousel_filter_tags',
		'label'       => esc_html__( 'Filter by Tags', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of tag slugs. For example: &laquo;worth-reading, top-5, playlists&raquo;. Leave empty for all tags.', 'quickstart' ),
		'section'     => 'carousel',
		'default'     => '',
		'priority'    => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'text',
		'settings'    => 'carousel_filter_posts',
		'label'       => esc_html__( 'Filter by Posts', 'quickstart' ),
		'description' => esc_html__( 'Add comma-separated list of post IDs. For example: 12, 34, 145. Leave empty for all posts.', 'quickstart' ),
		'section'     => 'carousel',
		'default'     => '',
		'priority'    => 10,
	)
);

if ( class_exists( 'Post_Views_Counter' ) ) {

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'     => 'radio',
			'settings' => 'carousel_orderby',
			'label'    => esc_html__( 'Order posts by', 'quickstart' ),
			'section'  => 'carousel',
			'default'  => 'date',
			'priority' => 10,
			'choices'  => array(
				'date'       => esc_html__( 'Date', 'quickstart' ),
				'post_views' => esc_html__( 'Views', 'quickstart' ),
			),
		)
	);

	CSCO_Kirki::add_field(
		'csco_theme_mod', array(
			'type'            => 'text',
			'settings'        => 'carousel_time_frame',
			'label'           => esc_html__( 'Filter by Time Frame', 'quickstart' ),
			'description'     => esc_html__( 'Add period of posts in English. For example: &laquo;2 months&raquo;, &laquo;14 days&raquo; or even &laquo;1 year&raquo;', 'quickstart' ),
			'section'         => 'carousel',
			'default'         => '',
			'priority'        => 10,
			'active_callback' => array(
				array(
					'setting'  => 'carousel_orderby',
					'operator' => '==',
					'value'    => 'post_views',
				),
			),
		)
	);
}

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'checkbox',
		'settings' => 'carousel_exclude',
		'label'    => esc_html__( 'Exclude posts of carousel from the main archive', 'quickstart' ),
		'section'  => 'carousel',
		'default'  => false,
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'        => 'checkbox',
		'settings'    => 'carousel_avoid_duplicate',
		'label'       => esc_html__( 'Exclude duplicate posts from the post carousel', 'quickstart' ),
		'description' => esc_html__( 'If enabled, posts from the upper sections will not be shown in the post carousel. The "Filter by Posts" option will override this option.', 'quickstart' ),
		'section'     => 'carousel',
		'default'     => false,
		'priority'    => 10,
	)
);

CSCO_Kirki::add_section(
	'sections_order', array(
		'title'    => esc_html__( 'Sections Order', 'quickstart' ),
		'panel'    => 'homepage_settings',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'sortable',
		'settings' => 'homepage_components',
		'label'    => esc_html__( 'Components', 'quickstart' ),
		'section'  => 'sections_order',
		'default'  => csco_homepage_components_default(),
		'choices'  => csco_homepage_components_choices(),
		'priority' => 10,
	)
);
