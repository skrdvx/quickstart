<?php
/**
 * Design
 *
 * @package Quickstart
 */

CSCO_Kirki::add_section(
	'design', array(
		'title'    => esc_html__( 'Design', 'quickstart' ),
		'priority' => 20,
	)
);

/**
 * -------------------------------------------------------------------------
 * Colors
 * -------------------------------------------------------------------------
 */

CSCO_Kirki::add_section(
	'design_base', array(
		'title'    => esc_html__( 'design', 'quickstart' ),
		'panel'    => 'design',
		'priority' => 10,
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'color',
		'settings' => 'color_primary',
		'label'    => esc_html__( 'Primary Color', 'quickstart' ),
		'section'  => 'design',
		'priority' => 10,
		'default'  => '#0721BA',
		'output'   => apply_filters( 'csco_color_primary', array(
			array(
				'element'  => 'a:hover, .must-log-in a, blockquote:before, .cs-bg-dark .pk-social-links-scheme-bold:not(.pk-social-links-scheme-light-rounded) .pk-social-links-link .pk-social-links-icon, .subscribe-title, .navbar-widgets button',
				'property' => 'color',
			),
			array(
				'element'  => 'button, input[type="button"], input[type="reset"], input[type="submit"], .button, .pk-subscribe-form-wrap .pk-input-group button',
				'property' => 'background-color',
			),
			array(
				'element'  => '.cs-list-articles > li > a:hover:before, .cs-video-tools .cs-player-state, .pk-bg-primary, .pk-button-primary, .pk-badge-primary, h2.pk-heading-numbered:before, .cs-bg-dark .pk-social-links-scheme-light-rounded .pk-social-links-link:hover .pk-social-links-icon, .post-sidebar-shares .pk-share-buttons-link .pk-share-buttons-count',
				'property' => 'background-color',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'      => 'color',
		'settings'  => 'color_overlay',
		'label'     => esc_html__( 'Overlay Color', 'quickstart' ),
		'section'   => 'design',
		'priority'  => 10,
		'default'   => 'rgba(0,0,0,0.25)',
		'transport' => 'auto',
		'choices'   => array(
			'alpha' => true,
		),
		'output'    => apply_filters( 'csco_color_overlay', array(
			array(
				'element'  => '.cs-overlay-background:after, .cs-overlay-hover:hover .cs-overlay-background:after, .cs-overlay-hover:focus .cs-overlay-background:after, .cs-overlay-original:after, .cs-overlay-hover:hover .cs-overlay-original:after, .cs-overlay-hover:focus .cs-overlay-original:after, .gallery-type-justified .gallery-item > .caption',
				'property' => 'background-color',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'            => 'color',
		'settings'        => 'color_large_header_bg',
		'label'           => esc_html__( 'Header Background', 'quickstart' ),
		'section'         => 'design',
		'default'         => '#FFFFFF',
		'priority'        => 10,
		'active_callback' => array(
			array(
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 'large',
				),
				array(
					'setting'  => 'header_layout',
					'operator' => '==',
					'value'    => 'with-top-bar',
				),
			),
		),
		'output'          => array(
			array(
				'element'  => '.header-large .navbar-topbar, .header-with-top-bar .navbar-topbar',
				'property' => 'background-color',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'color',
		'settings' => 'color_navbar_bg',
		'label'    => esc_html__( 'Navigation Bar Background', 'quickstart' ),
		'section'  => 'design',
		'default'  => '#FFFFFF',
		'priority' => 10,
		'output'   => array(
			array(
				'element'  => '.navbar-primary, .offcanvas-header',
				'property' => 'background-color',
			),
			array(
				'element'  => '.navbar-nav > .menu-item > a .pk-badge:after',
				'property' => 'border-color',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'color',
		'settings' => 'color_navbar_submenu',
		'label'    => esc_html__( 'Navigation Submenu Background', 'quickstart' ),
		'section'  => 'design',
		'default'  => '#FFFFFF',
		'priority' => 10,
		'output'   => array(
			array(
				'element'  => '.navbar-nav .menu-item:not(.cs-mega-menu) .sub-menu, .navbar-nav .cs-mega-menu-has-categories .cs-mm-categories, .navbar-primary .navbar-dropdown-container',
				'property' => 'background-color',
			),
			array(
				'element'  => '.navbar-nav > li.menu-item-has-children > .sub-menu:after, .navbar-primary .navbar-dropdown-container:after',
				'property' => 'border-bottom-color',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'     => 'color',
		'settings' => 'color_footer_bg',
		'label'    => esc_html__( 'Footer Background', 'quickstart' ),
		'section'  => 'design',
		'default'  => '#f6f6f9',
		'priority' => 10,
		'choices'  => array(
			'alpha' => true,
		),
		'output'   => array(
			array(
				'element'  => '.site-footer',
				'property' => 'background-color',
			),
		),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'dimension',
		'settings'          => 'design_border_radius',
		'label'             => esc_html__( 'Common Border Radius', 'quickstart' ),
		'description'       => esc_html__( 'For example: 30px. If the input is empty, original value will be used.', 'quickstart' ),
		'section'           => 'design',
		'default'           => '30px',
		'priority'          => 10,
		'sanitize_callback' => 'esc_html',
		'output'            => apply_filters( 'csco_design_border_radius', array(
			array(
				'element'  => '.cs-input-group input[type="search"]',
				'property' => 'border-radius',
			),
			array(
				'element'  => 'button, input[type="button"], input[type="reset"], input[type="submit"], input[type="text"], .wp-block-button:not(.is-style-squared) .wp-block-button__link, .button, .pk-button, .pk-scroll-to-top, .cs-overlay .post-categories a, .site-search [type="search"], .subcategories .cs-nav-link, .post-header .pk-share-buttons-wrap .pk-share-buttons-link, .pk-dropcap-borders:first-letter, .pk-dropcap-bg-inverse:first-letter, .pk-dropcap-bg-light:first-letter, .footer-instagram .instagram-username',
				'property' => 'border-radius',
			),
			array(
				'element'  => '.widget select, .widget-area .pk-subscribe-with-name input[type="text"], .widget-area .pk-subscribe-with-name button, .widget-area .pk-subscribe-with-bg input[type="text"], .widget-area .pk-subscribe-with-bg button',
				'property' => 'border-radius',
			),
			array(
				'element'     => '.pk-subscribe-with-name input[type="text"], .pk-subscribe-with-bg input[type="text"]',
				'property'    => 'border-radius',
				'media_query' => '@media (max-width: 599px)',
			),
			array(
				'element'  => '.cs-input-group input[type="search"], .pk-subscribe-form-wrap input[type="text"]:first-child',
				'property' => 'border-top-left-radius',
			),
			array(
				'element'  => '.cs-input-group input[type="search"], .pk-subscribe-form-wrap input[type="text"]:first-child',
				'property' => 'border-bottom-left-radius',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'dimension',
		'settings'          => 'secondary_border_radius',
		'label'             => esc_html__( 'Secondary Border Radius', 'quickstart' ),
		'description'       => esc_html__( 'For example: 10px. If the input is empty, original value will be used.', 'quickstart' ),
		'section'           => 'design',
		'default'           => '10px',
		'priority'          => 10,
		'sanitize_callback' => 'esc_html',
		'output'            => apply_filters( 'csco_design_preview_border_radius', array(
			array(
				'element'  => '.cs-hero, .cs-post-tiles-wrap, .post-wrap, .archive-full .entry-summary-content, .pk-post-item, .flickity-viewport',
				'property' => 'border-radius',
			),
			array(
				'element'     => '.cs-ratio-original .cs-overlay-original img',
				'property'    => 'border-radius',
				'media_query' => '@media (min-width: 720px)',
			),
		) ),
	)
);

CSCO_Kirki::add_field(
	'csco_theme_mod', array(
		'type'              => 'dimension',
		'settings'          => 'design_submenu_border_radius',
		'label'             => esc_html__( 'Submenu Border Radius', 'quickstart' ),
		'description'       => esc_html__( 'For example: 30px. If the input is empty, original value will be used.', 'quickstart' ),
		'section'           => 'design',
		'default'           => '10px',
		'priority'          => 10,
		'sanitize_callback' => 'esc_html',
		'output'            => apply_filters( 'csco_design_submenu_border_radius', array(
			array(
				'element'  => '.cs-hero, .cs-post-carousel-wrap, .post-wrap, .pk-post-item, .flickity-viewport, .widget_search .search-form, .powerkit_widget_author .widget-body, .widget_text .textwidget, .widget_nav_menu .menu-primary-container, .widget .pk-instagram-feed, .widget_recent_entries ul, .widget_rss ul, .widget_categories ul, .widget_pages ul, .widget_archive ul, .widget_meta ul, .widget_calendar .calendar_wrap',
				'property' => 'border-radius',
			),
		) ),
	)
);
