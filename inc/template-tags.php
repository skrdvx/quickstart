<?php
/**
 * Template Tags
 *
 * Functions that are called directly from template parts or within actions.
 *
 * @package Quickstart
 */

if ( ! function_exists( 'csco_page_pagination' ) ) {
	/**
	 * Post Pagination
	 */
	function csco_page_pagination() {
		if ( ! is_singular() ) {
			return;
		}

		do_action( 'csco_pagination_before' );

		wp_link_pages(
			array(
				'before'           => '<div class="navigation pagination"><div class="nav-links">',
				'after'            => '</div></div>',
				'link_before'      => '<span class="page-number">',
				'link_after'       => '</span>',
				'next_or_number'   => 'next_and_number',
				'separator'        => ' ',
				'nextpagelink'     => esc_html__( 'Next page', 'quickstart' ),
				'previouspagelink' => esc_html__( 'Previous page', 'quickstart' ),
			)
		);

		do_action( 'csco_pagination_after' );
	}
}

if ( ! function_exists( 'csco_the_post_format_icon' ) ) {
	/**
	 * Post Format Icon
	 *
	 * @param string $content After content.
	 */
	function csco_the_post_format_icon( $content = null ) {
		$post_format = get_post_format();

		if ( 'gallery' === $post_format ) {
			$attachments = count( (array) get_children( array(
				'post_parent' => get_the_ID(),
				'post_type'   => 'attachment',
			) ) );

			$content = $attachments ? sprintf( '<span>%s</span>', $attachments ) : '';
		}

		if ( $post_format ) {
			?>
			<span class="post-format-icon">
				<a class="cs-format-<?php echo esc_attr( $post_format ); ?>" href="<?php the_permalink(); ?>">
					<?php echo wp_kses( $content, 'post' ); ?>
				</a>
			</span>
			<?php
		}
	}
}

if ( ! function_exists( 'csco_single_tags' ) ) {
	/**
	 * Page Tags
	 */
	function csco_single_tags() {
		if ( ! is_single() ) {
			return;
		}

		if ( false === get_theme_mod( 'post_tags', true ) ) {
			return;
		}

		$tag = apply_filters( 'csco_section_title_tag', 'h5' );
		the_tags( '<section class="post-tags"><' . esc_html( $tag ) . ' class="title-tags">' . esc_html__( 'In this article:', 'quickstart' ) . '</' . esc_html( $tag ) . '>', ',', '</section>' );
	}
}

if ( ! function_exists( 'csco_archive_post_description' ) ) {
	/**
	 * Post Description in Archive Pages
	 */
	function csco_archive_post_description() {
		$description = get_the_archive_description();
		if ( $description ) {
			?>
			<div class="archive-description">
				<?php echo do_shortcode( $description ); ?>
			</div>
			<?php
		}
	}
}

if ( ! function_exists( 'csco_archive_post_count' ) ) {
	/**
	 * Post Count in Archive Pages
	 */
	function csco_archive_post_count() {
		global $wp_query;
		$found_posts = $wp_query->found_posts;
		?>
		<div class="archive-count">
			<?php
			/* translators: 1: Singular, 2: Plural. */
			echo esc_html( apply_filters( 'csco_article_full_count', sprintf( _n( '%s post', '%s posts', $found_posts, 'quickstart' ), $found_posts ), $found_posts ) );
			?>
		</div>
		<?php
	}
}

if ( ! function_exists( 'csco_post_details' ) ) {
	/**
	 * Post Details
	 *
	 * @param string $location The location meta.
	 */
	function csco_post_details( $location = 'post_meta' ) {
		if ( 'post' !== get_post_type() ) {
			return;
		}

		$post_author = csco_has_post_meta( 'author', $location );
		$post_date   = csco_has_post_meta( 'date', $location );

		if ( $post_author || $post_date ) {
		?>
			<div class="entry-author">
				<?php
				$authors_list = array();

				if ( $post_author ) {
					$authors = array( get_the_author_meta( 'ID' ) );

					if ( csco_coauthors_enabled() ) {
						$authors = csco_get_coauthors();
					}
				}

				if ( $post_author ) {
					foreach ( $authors as $author ) {
						$author_id   = isset( $author->ID ) ? $author->ID : $author;
						$author_name = isset( $author->display_name ) ? $author->display_name : get_the_author_meta( 'display_name', $author_id );

						$author_url = get_author_posts_url( $author_id, isset( $author->user_nicename ) ? $author->user_nicename : '' );

						$authors_list[] = sprintf( '<a href="%s">%s</a>', esc_url( $author_url ), esc_html( $author_name ) );

						if ( get_avatar( $author_id, 40 ) ) {
							?>
								<a class="author" href="<?php echo esc_url( $author_url ); ?>"><?php echo get_avatar( $author_id, 40 ); ?></a>
							<?php
						}
					}
				}
				?>
				<div class="entry-info">
					<?php
					if ( $authors_list ) {
						echo sprintf( '<div class="meta-author">%s</div>', wp_kses( implode( ', ', $authors_list ), 'post' ) );
					}

					if ( $post_date ) {
						csco_get_post_meta( array( 'date' ), false, true, $location );
					}
					?>
				</div>
		</div>
		<?php
		}
	}
}

if ( ! function_exists( 'csco_post_media_large' ) ) {
	/**
	 * Post Header Large
	 */
	function csco_post_media_large() {
		if ( ! is_singular() ) {
			return;
		}

		if ( ! has_post_thumbnail() ) {
			return;
		}

		if ( 'large' !== csco_get_page_header_type() ) {
			return;
		}

		$has_breadcrumbs = ! csco_doing_request() && csco_breadcrumbs( true, false );
		?>
		<div class="entry-media-large <?php echo esc_attr( $has_breadcrumbs ? 'has-breadcrumbs' : null ); ?>">
			<?php
			if ( $has_breadcrumbs ) {
			?>
				<div class="cs-container">
					<?php csco_breadcrumbs( true ); ?>
				</div>
			<?php } ?>

			<div class="cs-overlay-ratio cs-overlay-transparent cs-ratio-wide cs-video-wrap">

				<div class="entry-overlay cs-overlay-background">
					<?php
						the_post_thumbnail( 'csco-extra-large', array(
							'class' => 'pk-lazyload-disabled',
						) );
					?>
					<?php csco_get_video_background( 'large-header', null, false ); ?>
				</div>

				<?php
				if ( in_array( 'large-header', (array) get_post_meta( get_the_ID(), 'csco_post_video_location', true ), true ) ) {
					$video_url = get_post_meta( get_the_ID(), 'csco_post_video_url', true );
				}

				if ( isset( $video_url ) && $video_url ) {
				?>
					<div class="cs-video-tools-large">
						<a class="cs-player-control cs-player-link cs-player-stop" target="_blank" href="<?php echo esc_url( $video_url ); ?>">
							<span class="cs-tooltip"><span><?php esc_html_e( 'View on YouTube', 'quickstart' ); ?></span></span>
						</a>
						<span class="cs-player-control cs-player-volume cs-player-mute"></span>
						<span class="cs-player-control cs-player-state cs-player-pause"></span>
					</div>
				<?php } ?>

			</div>
		</div>
		<?php
	}
}

if ( ! function_exists( 'csco_post_media' ) ) {
	/**
	 * Post Media
	 */
	function csco_post_media() {
		$page_header = csco_get_page_header_type();

		if ( is_singular() && ! in_array( $page_header, array( 'standard', 'small' ), true ) ) {
			return;
		}

		if ( ! has_post_thumbnail() ) {
			return;
		}

		do_action( 'csco_post_media_before' );

		$caption = get_the_post_thumbnail_caption();

		$image_size = 'csco-medium';

		if ( 'disabled' === csco_get_page_sidebar() ) {
			$image_size = 'csco-large';
		}

		if ( 'uncropped' === csco_get_page_preview() ) {
			$image_size = sprintf( '%s-uncropped', $image_size );
		}

		if ( 'small' === $page_header ) {
			$image_size = 'csco-thumbnail-uncropped';

			if ( 'disabled' === csco_get_page_sidebar() ) {
				$image_size = 'csco-medium-uncropped';
			}
		}

		if ( is_singular() ) {
			$full_image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' );
			?>
			<div class="post-media">
				<figure <?php echo (string) $caption ? 'class="wp-caption"' : ''; ?>>
					<a href="<?php echo esc_url( $full_image ); ?>">
						<?php
							the_post_thumbnail( $image_size, array(
								'class' => 'pk-lazyload-disabled',
							) );
						?>
					</a>
					<?php if ( $caption ) { ?>
						<figcaption class="wp-caption-text"><?php the_post_thumbnail_caption(); ?></figcaption>
					<?php } ?>
				</figure>
			</div>
			<?php
		} else {
			?>
			<div class="post-media">
				<figure>
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( $image_size ); ?>
					</a>
				</figure>
			</div>
			<?php

		}
		do_action( 'csco_post_media_after' );
	}
}

if ( ! function_exists( 'csco_post_author' ) ) {
	/**
	 * Post Author Details
	 *
	 * @param int $id Author ID.
	 */
	function csco_post_author( $id = null ) {
		if ( ! $id ) {
			$id = get_the_author_meta( 'ID' );
		}

		$tag = apply_filters( 'csco_section_title_tag', 'h5' );
		?>
		<div class="author-wrap">
			<div class="author">
				<div class="author-data">
					<div class="author-avatar">
						<a href="<?php echo esc_url( get_author_posts_url( $id ) ); ?>" rel="author">
							<?php echo get_avatar( $id, '68' ); ?>
						</a>
					</div>
					<<?php echo esc_html( $tag ); ?> class="title-author">
						<span class="title-prefix"><?php esc_html_e( 'Post written by:', 'quickstart' ); ?></span>
						<span class="fn">
							<a href="<?php echo esc_url( get_author_posts_url( $id ) ); ?>" rel="author">
								<?php the_author_meta( 'display_name', $id ); ?>
							</a>
						</span>
					</<?php echo esc_html( $tag ); ?>>
				</div>

				<?php
				if ( get_the_author_meta( 'description', $id ) ) {
					?>
					<div class="author-description">
						<?php echo wp_kses_post( get_the_author_meta( 'description', $id ) ); ?>
					</div>
					<?php
				}
				?>

				<?php
				if ( powerkit_module_enabled( 'social_links' ) ) {
					?>
					<div class="author-social-accounts">
						<h5 class="author-social-label"><?php esc_html_e( 'Follow', 'quickstart' ); ?></h5>
						<?php powerkit_author_social_links( $id ); ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'csco_wrap_entry_content' ) ) {
	/**
	 * Wrap .entry-content content in div with a class.
	 *
	 * Used for floated share buttons on single posts.
	 */
	function csco_wrap_entry_content() {
		if ( 'post' !== get_post_type() ) {
			return;
		}

		if ( 'csco_post_content_before' === current_filter() ) {
			$tag = apply_filters( 'csco_share_title_tag', 'h5' );
			?>
			<div class="entry-container">
				<?php
				if ( is_singular( 'post' ) && csco_powerkit_module_enabled( 'share_buttons' ) ) {

					if ( powerkit_share_buttons_exists( 'post_sidebar' ) ) {
					?>
						<div class="entry-sidebar-wrap">
							<div class="entry-sidebar">
								<div class="post-sidebar-shares">
									<div class="post-sidebar-inner">
										<?php powerkit_share_buttons_location( 'post_sidebar' ); ?>
									</div>
								</div>
							</div>
						</div>
					<?php
					}
				}
		} else {
			?>
			</div>
			<?php
		}
	}
}
