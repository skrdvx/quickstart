<?php
/**
 * Widgets Init
 *
 * Register sitebar locations for widgets.
 *
 * @package Quickstart
 */

if ( ! function_exists( 'csco_widgets_init' ) ) {
	/**
	 * Register widget areas.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	 */
	function csco_widgets_init() {

		$tag = apply_filters( 'csco_section_title_tag', 'h5' );

		register_sidebar(
			array(
				'name'          => esc_html__( 'Default Sidebar', 'quickstart' ),
				'id'            => 'sidebar-main',
				'before_widget' => '<div class="widget %1$s %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="title-block-wrap"><' . $tag . ' class="title-block title-widget">',
				'after_title'   => '</' . $tag . '></div>',
			)
		);

		register_sidebar(
			array(
				'name'          => esc_html__( 'Off-Canvas', 'quickstart' ),
				'id'            => 'sidebar-offcanvas',
				'before_widget' => '<div class="widget %1$s %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="title-block-wrap"><' . $tag . ' class="title-block title-widget">',
				'after_title'   => '</' . $tag . '></div>',
			)
		);

		register_sidebar(
			array(
				'name'          => esc_html__( 'Navigation', 'quickstart' ),
				'id'            => 'sidebar-navigation',
				'before_widget' => '<div class="widget %1$s %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="title-block-wrap"><' . $tag . ' class="title-block title-widget">',
				'after_title'   => '</' . $tag . '></div>',
			)
		);

		register_sidebar(
			array(
				'name'          => esc_html__( 'Auto Loaded Sidebar', 'quickstart' ),
				'id'            => 'sidebar-loaded',
				'before_widget' => '<div class="widget %1$s %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="title-block-wrap"><' . $tag . ' class="title-block title-widget">',
				'after_title'   => '</' . $tag . '></div>',
			)
		);
	}
}
add_action( 'widgets_init', 'csco_widgets_init' );

if ( ! function_exists( 'csco_dynamic_sidebar_params' ) ) {
	/**
	 * Filters the parameters passed to a widget’s display callback.
	 *
	 * @param array $params An array of widget display arguments.
	 */
	function csco_dynamic_sidebar_params( $params ) {

		foreach ( $params as $key => $settings ) {

			if ( ! isset( $settings['widget_id'] ) || ! isset( $settings['before_widget'] ) ) {
				continue;
			}

			// Add support flickity to featured widget posts.
			if ( strpos( $settings['before_widget'], 'powerkit_widget_posts' ) ) {
				$params[ $key ]['before_widget'] = str_replace( 'widget ', 'widget cs-flickity-init ', $settings['before_widget'] );
			}
		}

		return $params;
	}
}
add_filter( 'dynamic_sidebar_params', 'csco_dynamic_sidebar_params' );

if ( ! function_exists( 'csco_widget_title_allow_html' ) ) {
	/**
	 * Add support <span class="title-style"> to widget title.
	 *
	 * @param string $title The widget title.
	 */
	function csco_widget_title_style( $title ) {

		$title = csco_add_support_title_style( $title );

		return $title;
	}
}
add_filter( 'widget_title', 'csco_widget_title_style' );
