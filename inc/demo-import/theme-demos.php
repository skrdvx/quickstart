<?php
/**
 * Theme Demos
 *
 * @package Quickstart
 */

/**
 * Theme Demos
 */
function csco_theme_demos() {
	$demos = array(
		// Theme mods imported with every demo.
		'common_mods' => array(),
		// Specific demos.
		'demos'       => array(
			'quickstart' => array(
				'name'              => 'quickstart',
				'preview_image_url' => '/images/theme-demos/logo-quickstart.png',
				'mods'              => array(
					'example' => true,
				),
				'mods_typekit'      => array(
					'example' => true,
				),
			),
		),
	);
	return $demos;
}
add_filter( 'csco_theme_demos', 'csco_theme_demos' );
