/**
 * Gulpfile.
 *
 * Gulp with WordPress.
 *
 * Implements:
 *      1. Live reloads browser with BrowserSync.
 *      2. CSS: Sass to CSS conversion, error catching, Autoprefixing, Sourcemaps,
 *         CSS minification.
 *      3. JS: Concatenates custom JS files.
 *      4. Watches files for changes in CSS or JS.
 *      5. Watches files for changes in PHP.
 *      6. Corrects the line endings.
 *      7. InjectCSS instead of browser page reload.
 *      8. Generates .pot file for i18n and l10n.
 *
 * @author Code Supply Co. (@codesupplyco)
 * @version 1.0.3
 */

/**
 * Load Config.
 *
 * Customize your project in the config.js file
 */
const config = require( './config-theme.js' );

/**
 * Load Plugins.
 *
 * Load gulp plugins and passing them semantic names.
 */
var gulp = require( 'gulp' ); // Gulp of-course

// Helper plugins.
var path = require( 'path' );

// CSS related plugins.
var sass         = require( 'gulp-sass' ); // Gulp pluign for Sass compilation.
var sassVars     = require( 'gulp-sass-vars' ); // Inject variables in sass files from a js object.
var autoprefixer = require( 'gulp-autoprefixer' ); // Autoprefixing magic.
var cssjanus     = require( 'gulp-cssjanus' ); // Converts CSS stylesheets between left-to-right and right-to-left.

// JS related plugins.
var concat   = require( 'gulp-concat' ); // Concatenates JS files.
var header   = require( 'gulp-header' ); //  Adds header to JS files.
var footer   = require( 'gulp-footer' ); // Adds footer to JS files.
var prettify = require( 'gulp-jsbeautifier' ); // Formats JS files according to the .jsbeautifyrc config file.
var jshint   = require( 'gulp-jshint' ); // JSHint.
var stylish  = require( 'jshint-stylish' ); // Stylish reported for JSHint.

// Utility related plugins.
var gulpif      = require('gulp-if');
var lineec      = require( 'gulp-line-ending-corrector' ); // Consistent Line Endings for non UNIX systems. Gulp Plugin for Line Ending Corrector (A utility that makes sure your files have consistent line endings)
var filter      = require( 'gulp-filter' ); // Enables you to work on a subset of the original files by filtering them using globbing.
var sourcemaps  = require( 'gulp-sourcemaps' ); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)
var notify      = require( 'gulp-notify' ); // Sends message notification to you
var browserSync = require( 'browser-sync' ).create(); // Reloads browser and injects CSS. Time-saving synchronised browser testing.
var wpPot       = require( 'gulp-wp-pot' ); // For generating the .pot file.
var sort        = require( 'gulp-sort' ); // Recommended to prevent unnecessary changes in pot-file.
var remember    = require( 'gulp-remember' ); //  Adds all the files it has ever seen back into the stream
var plumber     = require( 'gulp-plumber' ); // Prevent pipe breaking caused by errors from gulp plugins
var rename      = require( 'gulp-rename' );
var through     = require( 'through2' );

// Production related plugins.
var fs       = require('fs');
var archiver = require('archiver'); // Archive production theme files.


// Script Variables.
config.debug = true;

/**
 * Task: `copy`.
 *
 * Copy vendor files from node_modules to theme folder.
 *
 * This task does the following:
 *    1. Copies vendor JS files
 */
gulp.task( 'copy', function() {
	return gulp.src( config.vendorJS )
		.pipe( gulp.dest( './js/' ) );
} );

/**
 * Task: `browser-sync`.
 *
 * Live Reloads, CSS injections, Localhost tunneling.
 *
 * This task does the following:
 *    1. Sets the project URL
 *    2. Sets inject CSS
 *    3. You may define a custom port
 *    4. You may want to stop the browser from openning automatically
 */
function browsersync() {
	var	browserSyncDir = path.join( __dirname, '../../..' );

	var	browserSyncURL = path.basename( browserSyncDir ) + '.test';

	browserSync.init( {

		// For more options
		// @link http://www.browsersync.io/docs/options/

		// Proxy URL.
		proxy: browserSyncURL,

		// Host URL.
		host: browserSyncURL,

		// `true` Automatically open the browser with BrowserSync live server.
		// `false` Stop the browser from automatically opening.
		open: 'external',

		// Inject CSS changes.
		// Comment it to reload browser for every CSS change.
		injectChanges: true,

		// Don't show any notifications in the browser.
		notify: false,

		// Use a specific port (instead of the one auto-detected by Browsersync).
		port: 3000

	} );
}

// Helper function to allow browser reload with Gulp 4
function reload( done ) {
	browserSync.reload();
	done();
}

/**
 * Task: `styles`.
 *
 * Compiles Sass and Autoprefixes it.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates style.css
 *    5. Injects CSS or reloads the browser via browserSync
 *    6. Compiles RTL CSS.
 */
gulp.task( 'styles', function( callback ) {

	var modes = [ 'release', 'debug' ];

	// Loop.
	modes.forEach( function( mode ) {
		if ( 'debug' === mode ) {
			config.debug = true;
		} else {
			config.debug = false;
		}

		// Styles.
		gulp.src( config.styleSRC )
			.pipe( gulpif( config.debug, rename( { basename: 'style-dev' } ) ) )
			.pipe( gulpif( config.debug, sourcemaps.init() ) )
			.pipe( sass( {
				errLogToConsole: true,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				precision: 10
			} ) )
			.on( 'error', console.error.bind( console ) )
			.pipe( gulpif( ! config.debug, autoprefixer( {
				browsers: config.browsersList,
				cascade: false
			} ) ) )
			.pipe( gulpif( config.debug, sourcemaps.write( './' ) ) )
			.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
			.pipe( gulp.dest( config.styleDestination ) )
			.pipe( gulpif( config.debug, filter( '**/*.css' ) ) ) // Filtering stream to only css files
			.pipe( gulpif( config.debug, browserSync.stream() ) ) // Reloads style.css if that is enqueued.
			.pipe( gulpif( config.debug, notify( { message: 'style.css updated! 💯', onLast: true } ) ) );

		// Customizer Style.
		gulp.src( config.customizerStyleSRC )
			.pipe( gulpif( config.debug, rename( { basename: 'customizer-style-dev' } ) ) )
			.pipe( gulpif( config.debug, sourcemaps.init() ) )
			.pipe( sassVars( { rtl: false }, { verbose: true }) )
			.pipe( sass( {
				errLogToConsole: true,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				precision: 10
			} ) )
			.on( 'error', console.error.bind( console ) )
			.pipe( gulpif( ! config.debug, autoprefixer( {
				browsers: config.browsersList,
				cascade: false
			} ) ) )
			.pipe( gulpif( config.debug, sourcemaps.write( './' ) ) )
			.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
			.pipe( gulp.dest( config.customizerStyleDestination ) );

		// Editor Style.
		gulp.src( config.editorStyleSRC )
			.pipe( gulpif( config.debug, rename( { basename: 'editor-style-dev' } ) ) )
			.pipe( gulpif( config.debug, sourcemaps.init() ) )
			.pipe( sassVars( { rtl: false }, { verbose: true }) )
			.pipe( sass( {
				errLogToConsole: true,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				precision: 10
			} ) )
			.on( 'error', console.error.bind( console ) )
			.pipe( gulpif( ! config.debug, autoprefixer( {
				browsers: config.browsersList,
				cascade: false
			} ) ) )
			.pipe( gulpif( config.debug, sourcemaps.write( './' ) ) )
			.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
			.pipe( gulp.dest( config.editorStyleDestination ) );

		// Woocommerce Style.
		gulp.src( config.woocommerceStyleSRC )
			.pipe( gulpif( config.debug, rename( { basename: 'woocommerce-dev' } ) ) )
			.pipe( gulpif( config.debug, sourcemaps.init() ) )
			.pipe( sassVars( { rtl: false }, { verbose: true }) )
			.pipe( sass( {
				errLogToConsole: true,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				precision: 10
			} ) )
			.on( 'error', console.error.bind( console ) )
			.pipe( gulpif( ! config.debug, autoprefixer( {
				browsers: config.browsersList,
				cascade: false
			} ) ) )
			.pipe( gulpif( config.debug, sourcemaps.write( './' ) ) )
			.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
			.pipe( gulp.dest( config.woocommerceStyleDestination ) );

	} );

	/*
	 * -----------------------------------------------
	 * RTL Versions
	 * -----------------------------------------------
	 */

	// RTL.
	gulp.src( config.styleSRC )
		.pipe( sassVars( { rtl: true }, { verbose: true }) )
		.pipe( sass( {
			errLogToConsole: true,
			outputStyle: 'expanded',
			indentType: 'tab',
			indentWidth: 1,
			precision: 10
		} ) )
		.on( 'error', console.error.bind( console ) )
		.pipe( autoprefixer( {
			browsers: config.browsersList,
			cascade: false
		} ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( rename( { basename: 'style-rtl' } ) )
		.pipe( gulp.dest( config.styleDestination ) )
		.on( 'end', function(){
			gulp.src( config.styleDestination + 'style-rtl.css' )
				.pipe( cssjanus() )
				.pipe( gulp.dest( config.styleDestination ) );
		});

	// Editor RTL.
	gulp.src( config.editorStyleSRC )
		.pipe( sassVars( { rtl: true }, { verbose: true }) )
		.pipe( sass( {
			errLogToConsole: true,
			outputStyle: 'expanded',
			indentType: 'tab',
			indentWidth: 1,
			precision: 10
		} ) )
		.on( 'error', console.error.bind( console ) )
		.pipe( autoprefixer( {
			browsers: config.browsersList,
			cascade: false
		} ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( rename( { basename: 'editor-style-rtl' } ) )
		.pipe( gulp.dest( config.editorStyleDestination ) )
		.on( 'end', function(){
			gulp.src( config.editorStyleDestination + 'editor-style-rtl.css' )
				.pipe( cssjanus() )
				.pipe( gulp.dest( config.editorStyleDestination ) );
		});

	// Woocommerce RTL.
	gulp.src( config.woocommerceStyleSRC )
		.pipe( sassVars( { rtl: true }, { verbose: true }) )
		.pipe( sass( {
			errLogToConsole: true,
			outputStyle: 'expanded',
			indentType: 'tab',
			indentWidth: 1,
			precision: 10
		} ) )
		.on( 'error', console.error.bind( console ) )
		.pipe( autoprefixer( {
			browsers: config.browsersList,
			cascade: false
		} ) )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( rename( { basename: 'woocommerce-rtl' } ) )
		.pipe( gulp.dest( config.woocommerceStyleDestination ) )
		.on( 'end', function(){
			gulp.src( config.woocommerceStyleDestination + 'woocommerce-rtl.css' )
				.pipe( cssjanus() )
				.pipe( gulp.dest( config.woocommerceStyleDestination ) );
		});

	callback();
} );

/**
 * Task: `scripts`.
 *
 * Concatenate and uglify custom JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS custom files.
 *     2. Checks for errors with JSHint according to .jshintrc.
 *     3. Concatenates all the files and generates scripts.js.
 *     4. Prettifies all scripts.js according to .jsbeautifyrc.
 *     5. Fixes line endings.
 */
gulp.task( 'scripts', function() {
	return gulp.src( config.scriptsSRC, { since: gulp.lastRun( 'scripts' ) } ) // Only run on changed files.
		.pipe( plumber( {
			errorHandler: function( err ) {
				notify.onError( 'Error: <%= error.message %>' )( err );
				this.emit( 'end' ); // End stream if error is found
			}
		} ) )
		.pipe( jshint() )
		.pipe( jshint.reporter( stylish ) )
		.pipe( jshint.reporter( 'fail' ) )
		.pipe( remember( 'scripts' ) ) // Bring all files back to stream
		.pipe( concat( 'scripts.js' ) )
		.pipe( header('( function( $ ) {\n	"use strict";\n\n') )
		.pipe( footer('\n} )( jQuery );') )
		.pipe( prettify() )
		.pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
		.pipe( gulp.dest( config.scriptsDestination ) )
		.pipe( notify( { message: 'scripts.js updated! 💯', onLast: true } ) );
} );

/**
 * WP POT Translation File Generator.
 *
 * * This task does the following:
 *     1. Gets the source of all the PHP files
 *     2. Sort files in stream by path or any custom sort comparator
 *     3. Applies wpPot with the variable set at the top of this file
 *     4. Generate a .pot file of i18n that can be used for l10n to build .mo file
 */
gulp.task( 'translate', function() {
	return gulp.src( config.PHPWatchFiles )
		.pipe( sort() )
		.pipe( wpPot( {
			domain: config.textDomain,
			package: config.packageName,
			destFile: config.translationFile,
			bugReport: config.bugReport,
			lastTranslator: config.lastTranslator,
			team: config.team
		} ) )
		.pipe( gulp.dest( config.translationDestination + '/' + config.translationFile ) )
		.pipe( notify( { message: config.translationFile + ' updated! 💯', onLast: true } ) )
} );

/**
 * Watch Tasks.
 *
 * Watches for file changes and runs specific tasks.
 */
gulp.task(
	'watch',
	gulp.parallel(
		'styles',
		'scripts',
		browsersync,
		function watchFiles() {
			gulp.watch( config.PHPWatchFiles, reload ); // Reload on PHP file changes.
			gulp.watch( config.styleWatchFiles, gulp.parallel( 'styles' ) ); // Reload on SCSS file changes.
			gulp.watch( config.JSWatchFiles, gulp.series( 'scripts', reload ) ); // Reload on JS file changes.
		}
	)
);

/**
 * Build Task (Default).
 *
 * Builds styles and scripts and copies vendor scripts.
 */
gulp.task(
	'default',
	gulp.parallel(
		'copy',
		'styles',
		'scripts'
	)
);

/**
 * Zip task.
 *
 * Generates zip archive with production theme files.
 */
gulp.task( 'zip', function() {
	// Сreate a file to stream archive data to.
	var archive = archiver( 'zip' );

	// Pipe archive data to the file.
	archive.pipe( fs.createWriteStream( process.cwd() + '/' + config.textDomain + '.zip' ) );

	// Add parent dir.
	archive.append( null, { name: '/' + config.textDomain + '/' } );

	// Add files to archive.
	return gulp.src( config.themeFiles )
		.pipe( through.obj( function( file, enc, cb ) {
			if ( file.isNull() ) {
				if ( file.relative.length ) {
					archive.file( file.path, { name: config.textDomain + '/' + file.relative } );
				}
			} else {
				archive.append( file.contents, { name: config.textDomain + '/' + file.relative } );
			}
			cb();
		} ) )
		.on( 'end', function() {
			archive.finalize();

			notify( { message: config.textDomain + '.zip created! 💯', onLast: true } );
		} );
} );

/**
 * Archive Task.
 *
 * Creates production-ready archive with theme files.
 */
gulp.task(
	'archive',
	gulp.series(
		'default',
		'translate',
		'zip',
	)
);
