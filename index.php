<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Quickstart
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<?php do_action( 'csco_main_before' ); ?>

		<main id="main" class="site-main">

			<?php

			do_action( 'csco_main_start' );

			if ( have_posts() ) {
				$archive_layout = get_theme_mod( csco_get_archive_option( 'layout' ), 'masonry' );

				$type = 'mixed' === $archive_layout ? 'mixed' : 'default';
				?>

				<div class="post-archive">

					<div class="archive-wrap archive-type-<?php echo esc_attr( $type ); ?>">

						<?php
						if ( 'mixed' === $archive_layout ) {
							$counter = 1;

							$open = false;

							if ( 'disabled' === csco_get_page_sidebar() ) {
								$point_end = 7;
							} else {
								$point_end = 5;
							}

							$point_start = 1;

							// Start the Loop.
							while ( have_posts() ) {
								the_post();

								if ( ( $point_end + 1 ) === $counter ) {
									$counter = $point_start;
								}

								// Open grid layout.
								if ( $point_start === $counter ) {
									$open = true;
									?>
									<div class="archive-main archive-grid">
									<?php
								}

								// Open full layout.
								if ( $point_end === $counter ) {
									$open = true;
									?>
									<div class="archive-main archive-full">
									<?php
								}


								// Include template.
								if ( $counter <= ( $point_end - 1 ) ) {
									get_template_part( 'template-parts/content' );
								} else {
									get_template_part( 'template-parts/content-singular' );
								}


								// Close grid or full layout.
								if ( ( $point_end - 1 ) === $counter || $point_end === $counter ) {
									$open = false;
									?>
									</div>
									<?php
								}

								$counter++;
							}

							// Close the open tag.
							if ( $open ) {
								?>
								</div>
								<?php
							}
						} else {
						?>

							<div class="archive-main archive-<?php echo esc_attr( $archive_layout ); ?>">
								<?php
								// Start the Loop.
								while ( have_posts() ) {
									the_post();

									if ( 'full' === $archive_layout ) {
										get_template_part( 'template-parts/content-singular' );
									} else {
										get_template_part( 'template-parts/content' );
									}
								}

								// Columns for masonry.
								if ( 'masonry' === $archive_layout ) {
									echo '<div class="archive-col archive-col-1"></div>';
									echo '<div class="archive-col archive-col-2"></div>';
									echo '<div class="archive-col archive-col-3"></div>';
								}
								?>
							</div>

						<?php } ?>

					</div>

					<?php
					/* Posts Pagination */
					if ( 'standard' === get_theme_mod( csco_get_archive_option( 'pagination_type' ), 'load-more' ) ) {
						the_posts_pagination(
							array(
								'prev_text' => esc_html__( 'Previous', 'quickstart' ),
								'next_text' => esc_html__( 'Next', 'quickstart' ),
							)
						);
					}
					?>

				</div>

			<?php
			} else {
				?>

				<div class="entry-content content-not-found">
					<p><?php esc_html_e( 'It seems we cannot find what you are looking for. Perhaps searching can help.', 'quickstart' ); ?></p>
					<?php get_search_form(); ?>
				</div>

				<?php
			}

			do_action( 'csco_main_end' );
			?>

		</main>

		<?php do_action( 'csco_main_after' ); ?>

	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
