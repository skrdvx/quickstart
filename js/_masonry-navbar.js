/**
 * Masonry Widget Area
 */

function initmasonryNavbar() {

	/**
	 * Masonry Options
	 */
  var masonryNavbar = $('.navbar-area'),
    masonryNavbarOptions = {
      columns: '.widget-col',
      items: '.widget'
    };

  // Set Masonry.
  $(masonryNavbar).imagesLoaded(function () {
    $(masonryNavbar).colcade(masonryNavbarOptions);
  });
}

$(document).ready(function () {
  initmasonryNavbar();
});
