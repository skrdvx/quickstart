( function() {
	var viewport = $( window ).height();
	var lastScrollY = 0;
	var ticking     = false;
	var offset      = 200;

	var update = function() {
		var article  = $( '.single-post .site-main > article' );

		if ( 1 === article.length ) {
			let articleHeight = $( article ).innerHeight();

			// Pagination points.
			let topPoint    = $( article ).offset().top;
			let bottomPoint = topPoint + articleHeight + offset;

			if ( lastScrollY > topPoint && lastScrollY + viewport < bottomPoint ) {
				$( '.post-prev-next-along' ).addClass( 'pagination-visible' );
			} else {
				$( '.post-prev-next-along' ).removeClass( 'pagination-visible' );
			}
		}

		ticking = false;
	};

	var requestTick = function() {
		if ( ! ticking ) {
			window.requestAnimationFrame( update );
			ticking = true;
		}
	};

	var onProcess = function() {
		lastScrollY = window.scrollY;

		requestTick();
	};

	$( window ).on( 'scroll', onProcess );
	$( window ).on( 'resize', onProcess );
	$( window ).on( 'slider-refresh', onProcess );

} )();
