/**
 * Masonry Widget Area
 */

function initMasonrySidebar() {

	/**
	 * Masonry Options
	 */
	var masonrySidebar = $( '.sidebar-area' ),
		masonrySidebarOptions = {
			columns: '.sidebar',
			items: '.widget'
		};

	// Set Masonry.
	$( masonrySidebar ).imagesLoaded( function() {
		$( masonrySidebar ).colcade( masonrySidebarOptions );
	} );
}

$( document ).ready( function() {
	initMasonrySidebar();
} );
