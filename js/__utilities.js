// Get notified when a DOM element enters or exits the viewport.
$.fn.isInViewport = function() {
	var elementTop    = $( this ).offset().top;
	var elementBottom = elementTop + $( this ).outerHeight();

	var viewportTop    = $( window ).scrollTop();
	var viewportBottom = viewportTop + $( window ).height();

	return elementBottom > viewportTop && elementTop < viewportBottom;
};

// Initialization objectFitImages.
$( document ).ready( function() {
	objectFitImages();
} );
