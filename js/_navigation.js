/*
 * ----------------------------------------------------------------------------
 * Navigation
 */

var cscoNavigation = {};

( function() {
	var $this;

	cscoNavigation = {
		sScrollAllow : false,
		sInFirst     : true,
		sInterval    : 0,
		sPrevious    : 0,
		sDirection   : 0,

		loadStickyOffset : 0,
		loadAdminBar     : false,

		Sticky            : $( 'body' ).hasClass( 'navbar-sticky-enabled' ),
		StickyUp          : $( 'body' ).hasClass( 'navbar-smart-enabled' ),
		StickyNav         : $( '.site-header .navbar-primary' ),
		StickyHeader      : $( '.site-header' ),
		StickyOffsetType  : 'auto',
		StickyOffset      : 0,
		StickyOffsetFull  : 0,

		/*
		 * Initialize
		 */
		init: function( e ) {
			$this = cscoNavigation;

			// Init events.
			$this.events( e );
		},

		/*
		 * Events
		 */
		events: function( e ) {
			// DOM Load
			document.addEventListener( 'DOMContentLoaded', function(e) {
				$this.stickyInit( e );
				$this.smartLevels( e );
				$this.adaptTablet( e );
			});
			// Resize
			window.addEventListener( 'resize', function( e ) {
				$this.stickyInit( e );
				$this.smartLevels( e );
				$this.adaptTablet( e );
			} );
			// Scroll
			window.addEventListener( 'scroll', function( e ) {
				window.requestAnimationFrame( $this.stickyScroll );
			} );

			// Add dynamic search support.
			$( document ).on( 'animate-search', function( e ) {
				$this.animateSearch( e );
			} );
			$( document ).on( 'animate-search-start', function( e ) {
				$this.animateSearchStart( e );
			} );
			$( document ).on( 'animate-search-done', function( e ) {
				$this.animateSearchDone( e );
			} );
		},

		/*
		 * Init nav bar sticky
		 */
		stickyInit: function( e ) {

			if ( ! $this.Sticky ) {
				return;
			}

			$this.sScrollAllow = false;

			// Calc sticky offset.
			if ( $this.StickyOffsetType !== 'size' ) {

				var calcbar = 0;
				var wpadminbar = 0;

				if ( $( '#wpadminbar' ).length > 0 ) {
					calcbar = $( '#wpadminbar' ).outerHeight();

					wpadminbar = calcbar;

					if ( 'resize' !== e.type ) {
						$this.loadAdminBar = wpadminbar;
					}

					if ( 'absolute' === $( '#wpadminbar' ).css( 'position' ) ) {
						wpadminbar = 0;

						if ( 'resize' !== e.type ) {
							$this.loadAdminBar = 0;
						}
					}
				}

				// Calc outside header.
				$this.StickyOffsetFull = $this.StickyHeader.outerHeight();

				// Calc on load offset top.
				var elOffset = $this.StickyNav.not( '.sticky-nav' ).offset();

				if ( elOffset && ! $this.StickyNav.hasClass( '.sticky-nav' ) ) {

					$this.StickyOffset = elOffset.top;

					$this.loadStickyOffset = elOffset.top;
				} else {
					$this.StickyOffset = $this.loadStickyOffset;
				}

				// Consider the size of the wpadminbar.
				if ( 32 === $this.loadAdminBar ) {
					if ( 46 === calcbar ) {
						$this.StickyOffset = $this.StickyOffset - wpadminbar + 14;
					} else {
						$this.StickyOffset = $this.StickyOffset - wpadminbar;
					}
				} else if ( 46 === $this.loadAdminBar || 0 === $this.loadAdminBar ) {

					if ( 32 === calcbar ) {
						$this.StickyOffset = $this.StickyOffset - wpadminbar - 14;
					} else {
						$this.StickyOffset = $this.StickyOffset - wpadminbar;
					}
				}
			}

			// Nav Height.
			var navHeight = $this.StickyNav.outerHeight();

			// Set the min-height default of the header.
			$this.StickyHeader.data( 'min-height', $this.StickyOffsetFull - navHeight );

			// Document ready.
			if ( 'resize' !== e.type ) {

				// Add nav dummy.
				$this.StickyNav.after( '<div class="navbar-dummy"></div>' );
				$this.StickyHeader.find( '.navbar-dummy' ).height( navHeight );

				// Set type slide.
				if ( $this.StickyUp ) {
					$this.StickyHeader.addClass( 'sticky-type-slide' );
				}
			}

			// Allow.
			$this.sScrollAllow = true;
		},

		/*
		 * Make nav bar sticky
		 */
		stickyScroll: function( e ) {
			if ( ! $this.sScrollAllow ) {
				return;
			}

			var scrollCurrent = $( window ).scrollTop();

			if ( $this.StickyUp ) {

				if ( scrollCurrent > $this.StickyOffsetFull ) {
					$this.StickyNav.addClass( 'sticky-nav' );
				}

				if ( scrollCurrent <= $this.StickyOffset ) {
					$this.StickyNav.removeClass( 'sticky-nav' );
				}

				// Set scroll temporary vars.
				if ( scrollCurrent > $this.sPrevious ) {
					$this.sInterval = 0;
					$this.sDirection = 'down';

					$this.StickyNav.addClass( 'sticky-down' ).removeClass( 'sticky-up' );
				} else {
					$this.sInterval += $this.sPrevious - scrollCurrent;
					$this.sDirection = 'up';

					$this.StickyNav.addClass( 'sticky-up' ).removeClass( 'sticky-down' );
				}

				// Сonditions.
				if ( $this.sInterval > 150 && 'up' === $this.sDirection ) {
					$this.StickyNav.addClass( 'sticky-nav-slide-visible' );

					$( document ).trigger( 'sticky-nav-visible' );
				} else {
					$this.StickyNav.removeClass( 'sticky-nav-slide-visible' );

					$( document ).trigger( 'sticky-nav-hide' );
				}

				if ( scrollCurrent > $this.StickyOffsetFull + 150 ) {
					$this.StickyNav.addClass( 'sticky-nav-slide' );
				} else {
					$this.StickyNav.removeClass( 'sticky-nav-slide' );
				}

				// Show onload document.
				if ( $this.sInFirst && scrollCurrent > $this.StickyOffsetFull + 150 ) {
					$this.StickyNav.addClass( ' sticky-nav-slide-visible sticky-up' );
					$this.StickyNav.addClass( 'sticky-nav-slide' );

					$( document ).trigger( 'sticky-nav-visible' );

					$this.sDirection = 'up';
					$this.sInterval  = 151;
					$this.sInFirst   = false;
				}
			} else {
				// Сonditions.
				if ( scrollCurrent > $this.StickyOffset ) {
					$this.StickyNav.addClass( 'sticky-nav' );

					$( document ).trigger( 'sticky-nav-visible' );
				} else {
					$this.StickyNav.removeClass( 'sticky-nav' );
					$( document ).trigger( 'sticky-nav-hide' );
				}
			}

			$this.sPrevious = scrollCurrent;
		},

		/*
		 * Dynamic search support
		 */
		animateSearch: function( e ) {
			var minHeightMain = $this.StickyHeader.data( 'min-height' );
			var navHeight     = $this.StickyNav.outerHeight();

			$this.StickyOffsetFull = ( minHeightMain + navHeight );

			$this.StickyHeader.find( '.navbar-dummy' ).height( navHeight );
		},

		/*
		 * Start: Dynamic search support
		 */
		animateSearchStart: function( e ) {
			$this.sScrollAllow = false;
		},

		/*
		 * Done: Dynamic search support
		 */
		animateSearchDone: function( e ) {
			var minHeightMain = $this.StickyHeader.data( 'min-height' );
			var navHeight     = $this.StickyNav.outerHeight();

			$this.sScrollAllow = true;

			$this.sPrevious = $this.sPrevious + ( navHeight - minHeightMain );
		},

		/*
		 * Smart multi-Level menu
		 */
		smartLevels: function( e ) {

			var windowWidth = $( window ).width();

			// Reset Calc.
			$( '.navbar-nav li' ).removeClass( 'cs-mm-level' );
			$( '.navbar-nav li' ).removeClass( 'cs-mm-position-left cs-mm-position-right' );
			$( '.navbar-nav li .sub-menu' ).removeClass( 'cs-mm-position-init' );

			// Set Settings.
			$( '.navbar-nav > li.menu-item' ).not( '.cs-mega-menu' ).each( function( index, parent ) {
				var position = 'cs-mm-position-right';
				var objPrevWidth = 0;

				$( parent ).find( '.sub-menu' ).each( function( index, el ) {

					// Reset child levels.
					$( el ).parent().next( 'li' ).addClass( 'cs-mm-level' );

					if ( $( el ).parent().hasClass( 'cs-mm-level' ) ) {

						$( el ).parent().removeClass( 'cs-mm-level' );

						position = 'cs-mm-position-right';
						objPrevWidth = 0;
					}

					// Find out position items.
					var offset = $( el ).offset();
					var objOffset = offset.left;

					if ( 'cs-mm-position-right' === position && $( el ).outerWidth() + objOffset > windowWidth ) {
						position = 'cs-mm-position-left';
					}

					if ( 'cs-mm-position-left' === position && objOffset - ( $( el ).outerWidth() + objPrevWidth ) < 0 ) {
						position = 'cs-mm-position-right';
					}

					objPrevWidth = $( el ).outerWidth();

					$( el ).addClass( 'cs-mm-position-init' ).parent().addClass( position );
				} );

			} );
		},

		/*
		 * Adapting nav bar for tablet
		 */
		adaptTablet: function( e ) {
			// Click outside.
			$( document ).on( 'touchstart', function( e ) {

				if ( !$( e.target ).closest( '.navbar-nav' ).length ) {
					$( '.navbar-nav .menu-item-has-children' ).removeClass( 'submenu-visible' );
				} else {
					$( e.target ).parents( '.menu-item' ).siblings().find( '.menu-item' ).removeClass( 'submenu-visible' );
					$( e.target ).parents( '.menu-item' ).siblings().closest( '.menu-item' ).removeClass( 'submenu-visible' );
				}
			} );

			$( '.navbar-nav .menu-item-has-children' ).each( function( e ) {

				// Reset class.
				$( this ).removeClass( 'submenu-visible' );

				// Remove expanded.
				$( this ).find( '> a > .expanded' ).remove();

				// Add a caret.
				if ( 'ontouchstart' in document.documentElement ) {
					$( this ).find( '> a' ).append( '<span class="expanded"></span>' );
				}

				// Check touch device.
				$( this ).addClass( 'ontouchstart' in document.documentElement ? 'touch-device' : '' );

				$( '> a .expanded', this ).on( 'touchstart', function( e ) {
					e.preventDefault();

					$( this ).closest( '.menu-item-has-children' ).toggleClass( 'submenu-visible' );
				} );


				if ( '#' === $( '> a', this ).attr( 'href' ) ) {
					$( '> a', this ).on( 'touchstart', function( e ) {
						e.preventDefault();

						if ( !$( e.target ).hasClass( 'expanded' ) ) {
							$( this ).closest( '.menu-item-has-children' ).toggleClass( 'submenu-visible' );
						}
					} );
				}
			} );
		}
	};

} )();

// Initialize.
cscoNavigation.init();
