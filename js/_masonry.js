/**
 * Masonry Archive
 */

function initMasonry() {

	var masonryArchive = $( '.archive-masonry' ),
		masonryArchiveOptions = {
			columns: '.archive-col',
			items: 'article'
		};

	$( masonryArchive ).imagesLoaded( function() {
		$( masonryArchive ).colcade( masonryArchiveOptions );
	} );
}

$( document ).ready( function() {
	initMasonry();
} );
