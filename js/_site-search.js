/**
 * Site Search
 */

function triggerSearch() {
	$( document ).trigger( 'animate-search-start' );

	// Start repeats at intervals of 25 milliseconds.
	var timerSearchId = setInterval( function() {
		$( document ).trigger( 'animate-search' );
	}, 25 );

	// After 400 milliseconds sec stop replays.
	setTimeout( function() {
		clearInterval( timerSearchId );
	}, 399 );

	setTimeout( function() {
		$( document ).trigger( 'animate-search-done' );
	}, 400 );
}

$( '.toggle-search' ).on( 'click', function( event ) {

	event.preventDefault();

	var container = $( '#search' ),
	  field = $( 'input[type="search"]', container );

	// Toggle class on search button.
	$( this ).toggleClass( 'toggle-close' );

	// Add class to container.
	container.toggleClass( 'search-open' );

	// Slide toggle the container.
	triggerSearch();

	// Focus on / blur input field.
	if ( container.hasClass( 'search-open' ) ) {
		field.focus();
	} else {
		field.blur();
	}
} );

$( '#search, .search-close' ).on( 'click keyup', function( event ) {
	// Fire only when pressing Escape or clicking the .search-toggle button.
	if ( event.target.className === 'search-close' || event.keyCode === 27 ) {
		event.preventDefault();
		// Toggle class on search button.
		$( '.toggle-search' ).removeClass( 'toggle-close' );

		// Remove class from container.
		$( '#search' ).removeClass( 'search-open' );

		$( '#search input[type="search"]' ).blur();
		// Slide toggle the container.
		triggerSearch();
	}
} );
