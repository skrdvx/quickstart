/**
 * Post Carousel
 */
$( '.cs-post-carousel-items' ).imagesLoaded( function( instance ) {

	var rtl = $( 'body' ).hasClass( 'rtl' ) ? true : false;

	$( instance.elements ).each( function( index, el ) {

		$( el ).flickity( {
			cellAlign: rtl ? 'right' : 'left',
			groupCells: true,
			wrapAround: true,
			contain: true,
			autoPlay: false,
			adaptiveHeight: false,
			prevNextButtons: false,
			pageDots: false,
			rightToLeft: rtl,
			on: {
				ready: function() {
					$( el ).addClass( 'is-animate' );
				}
			}
		} );

		var container = $( el ).parents( '.cs-post-carousel' );

		// Select the previous slide.
		$( container ).find( '.carousel-previous' ).on( 'click', function( event ) {
			event.preventDefault();

			var type = rtl ? 'next' : 'previous';

			$( el ).flickity( 'stopPlayer' ).flickity( type, true ).flickity( 'playPlayer' );
		});

		// Select the next slide.
		$( container ).find( '.carousel-next' ).on( 'click', function( event ) {
			event.preventDefault();

			var type = rtl ? 'previous' : 'next';

			$( el ).flickity( 'stopPlayer' ).flickity( type, true ).flickity( 'playPlayer' );
		});
	} );

} );

/**
 * Footer Slider
 */
$( '.footer-instagram .pk-alt-instagram-items' ).imagesLoaded( function( instance ) {

	// Set unique id.
	var requestId;

	// Set rtl status.
	var rtl = $( 'body' ).hasClass( 'rtl' ) ? true : false;

	$( instance.elements ).each( function( index, el ) {

		const mainTicker = new Flickity( el, {
			freeScroll: true,
			accessibility: true,
			resize: true,
			wrapAround: true,
			prevNextButtons: false,
			pageDots: false,
			percentPosition: true,
			setGallerySize: true,
			adaptiveHeight: true,
			rightToLeft: rtl,
			on: {
				ready: function() {
					$( el ).addClass( 'is-animate' );
				}
			}
		} );

		// Set initial position to be 0
		mainTicker.x = 0;

		// Main function that 'plays' the marquee.
		function flickity_play() {
			// Set the decrement of position x
			mainTicker.x = mainTicker.x - 1.5;


			// Settle position into the slider
			mainTicker.settle( mainTicker.x );

			// Set the requestId to the local variable
			requestId = window.requestAnimationFrame( flickity_play );
		}

		// Main function to cancel the animation.
		function flickity_pause() {
			if ( requestId ) {
				// Cancel the animation.
				window.cancelAnimationFrame( requestId );

				requestId = undefined;
			}
		}

		// Start the marquee animation.
		$( document ).ready( function() {
			flickity_play();
		} );

		// Pause on hover/focus.
		$( el ).on( 'mouseenter focusin', e => {
			flickity_pause();
		} );

		// Unpause on mouse out / defocus.
		$( el ).on( 'mouseleave', e => {
			flickity_play();
		} );

	} );

} );