( function() {
	var initAPI = false;
	var process = false;
	var contex  = [];
	var players = [];
	var attrs   = [];

	// Create deferred object
	var YTdeferred = $.Deferred();
	window.onYouTubePlayerAPIReady = function() {
		// Resolve when youtube callback is called
		// passing YT as a parameter.
		YTdeferred.resolve( window.YT );
	};

	// Embedding youtube iframe api.
	function embedYoutubeAPI() {
		var tag = document.createElement( 'script' );
		tag.src = 'https://www.youtube.com/iframe_api';
		var firstScriptTag = document.getElementsByTagName( 'script' )[ 0 ];
		firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );
	}

	// Video rescale.
	function rescaleVideoBackground() {
		$( '.cs-video-init' ).each( function() {
			let w = $( this ).parent().width();
			let h = $( this ).parent().height();

			var hideControl = 400;

			let id = $( this ).attr( 'data-uid' );

			if ( w / h > 16 / 9 ) {
				players[ id ].setSize( w, w / 16 * 9 + hideControl );
			} else {
				players[ id ].setSize( h / 9 * 16, h + hideControl );
			}
		});
	}

	// Init video background.
	function initVideoBackground() {

		if ( process ) {
			return;
		}

		process = true;

		// Smart init API.
		if ( ! initAPI  ) {
			let elements = $( '.cs-video-wrapper[data-video-id]' );

			if ( elements.length ) {
				embedYoutubeAPI();

				initAPI = true;
			}
		}

		if ( ! initAPI ) {
			process = false;

			return;
		}

		// Whenever youtube callback was called = deferred resolved
		// your custom function will be executed with YT as an argument.
		YTdeferred.done( function( YT ) {

			$( '.cs-video-inner' ).each( function() {

				// The state.
				var isInit = $( this ).hasClass( 'cs-video-init' );

				var id = null;

				// Generate unique ID.
				if ( ! isInit ) {
					id = Math.random().toString( 36 ).substr( 2, 9 );
				} else {
					id = $( this ).attr( 'data-uid' );
				}

				// Create contex.
				contex[ id ] = this;

				// The actived.
				var isActive = $( contex[ id ] ).hasClass( 'active' );

				// The monitor.
				var isInView = $( contex[ id ] ).isInViewport();

				// Initialization.
				if ( isInView && ! isInit ) {
					// Add init class.
					$( contex[ id ] ).addClass( 'cs-video-init' );

					// Add unique ID.
					$( contex[ id ] ).attr( 'data-uid', id );

					// Get video attrs.
					let videoID = $( contex[ id ] ).parent().data( 'video-id' );
					let videoStart = $( contex[ id ] ).parent().data( 'video-start' );
					let videoEnd = $( contex[ id ] ).parent().data( 'video-end' );

					// Check video id.
					if ( typeof videoID === 'undefined' || ! videoID ) {
						return;
					}

					// Video attrs.
					attrs[ id ] = {
						'videoId': videoID,
						'startSeconds': videoStart,
						'endSeconds': videoEnd,
						'suggestedQuality': 'hd720'
					};

					// Creating a player.
					players[ id ] = new YT.Player( contex[ id ], {
						playerVars: {
							autoplay: 0,
							autohide: 1,
							modestbranding: 1,
							rel: 0,
							showinfo: 0,
							controls: 0,
							disablekb: 1,
							enablejsapi: 0,
							iv_load_policy: 3,
							playsinline: 1,
							loop: 1,
						},
						events: {
							'onReady': function() {
								players[ id ].loadVideoById( attrs[ id ] );
								players[ id ].mute();
							},
							'onStateChange': function( e ) {
								if ( e.data === 1 ) {
									$( e.target.a ).parents( '.cs-overlay, .cs-video-wrap' ).addClass( 'cs-video-bg-init' );
									$( e.target.a ).addClass( 'active' );
								} else if ( e.data === 0 ) {
									players[ id ].seekTo( attrs[ id ].startSeconds );
								}
							}
						}
					} );
					console.log(players[ id ]);
					rescaleVideoBackground();
				}

				// Pause and play.
				let control = $( contex[ id ] ).parents( '.cs-overlay, .cs-video-wrap' ).find( '.cs-player-state' );

				if ( isActive && isInit && ! $( control ).hasClass( 'cs-player-upause' ) ) {

					if ( isInView && $( control ).hasClass( 'cs-player-play' ) ) {
						// Change icon.
						$( control ).removeClass( 'cs-player-play' ).addClass( 'cs-player-pause' );
						// Pause video.
						players[ id ].playVideo();
					}

					if ( ! isInView && $( control ).hasClass( 'cs-player-pause' ) ) {
						// Change icon.
						$( control ).removeClass( 'cs-player-pause' ).addClass( 'cs-player-play' );
						// Pause video.
						players[ id ].pauseVideo();
					}
				}
			} );
		} );

		process = false;
	}

	// State Control.
	$( document ).on( 'click', '.cs-player-state', function() {
		let container = $( this ).parents( '.cs-overlay, .cs-video-wrap' ).find( '.cs-video-inner' );

		let id = $( container ).attr( 'data-uid' );

		$( this ).toggleClass( 'cs-player-pause cs-player-play' );

		if ( $( this ).hasClass( 'cs-player-pause' ) ) {
			$( this ).removeClass( 'cs-player-upause' );
			players[ id ].playVideo();
		} else {
			$( this ).addClass( 'cs-player-upause' );
			players[ id ].pauseVideo();
		}
	});

	// Stop Control.
	$( document ).on( 'click', '.cs-player-stop', function() {
		let container = $( this ).parents( '.cs-overlay, .cs-video-wrap' ).find( '.cs-video-inner' );

		let id = $( container ).attr( 'data-uid' );

		$( this ).siblings('.cs-player-state').removeClass( 'cs-player-pause' ).addClass( 'cs-player-play' );

		$( this ).siblings('.cs-player-state').addClass( 'cs-player-upause' );

		players[ id ].pauseVideo();
	});

	// Volume Control.
	$( document ).on( 'click', '.cs-player-volume', function() {
		let container = $( this ).parents( '.cs-overlay, .cs-video-wrap' ).find( '.cs-video-inner' );

		let id = $( container ).attr( 'data-uid' );

		$( this ).toggleClass( 'cs-player-mute cs-player-unmute' );

		if ( $( this ).hasClass( 'cs-player-unmute' ) ) {
			players[ id ].unMute();
		} else {
			players[ id ].mute();
		}
	});

	// Document scroll.
	$( window ).on( 'load scroll resize scrollstop' , function() {
		initVideoBackground();
	});

	// Document ready.
	$( document ).ready( function() {
		initVideoBackground();
	} );

	// Post load.
	$( document.body ).on( 'post-load', function() {
		initVideoBackground();
	} );

	// Document resize.
	$( window ).on( 'resize', function() {
		rescaleVideoBackground();
	} );

	// Init.
	initVideoBackground();

} )();
