//
// Mobile Menu
//

$( '.toggle-offcanvas, .site-overlay' ).on( 'click', function( e ) {
	e.preventDefault();

	// Transition.
	if ( ! $( 'body' ).hasClass( 'offcanvas-active' ) ) {
		$( 'body' ).addClass( 'offcanvas-transition' );
	} else {
		setTimeout( function() {
			$( 'body' ).removeClass( 'offcanvas-transition' );
		}, 400 );
	}

	// Toogle offcanvas.
	$( 'body' ).toggleClass( 'offcanvas-active' );
} );
