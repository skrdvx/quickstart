/**
 * AJAX Auto Load Next Post.
 *
 * Contains functions for AJAX Auto Load Next Post.
 */


/**
 * Check if Load Nextpost is defined by the wp_localize_script
 */
if ( typeof csco_ajax_nextpost !== 'undefined' ) {

	var objNextparent   = $( '.site-primary > .site-content' ),
		objNextsect     = '.cs-nextpost-section',
		objNextpost     = null,
		currentNTitle   = document.title,
		currentNLink    = window.location.href,
		loadingNextpost = false,
		scrollNextpost  = {
			allow: true,
			reallow: function() {
				scrollNextpost.allow = true;
			},
			delay: 400 //(milliseconds) adjust to the highest acceptable value
		};

	// Init.
	if ( csco_ajax_nextpost.next_post ) {
		$( objNextparent ).after( '<div class="cs-nextpost-inner"></div>' );

		objNextpost = $( '.cs-nextpost-inner' );
	}
}

/**
 * Get next post
 */
function csco_ajax_get_nextpost() {
	loadingNextpost = true;

	// Set class loading.
	var data = {
		action: 'csco_ajax_load_nextpost',
		not_in: csco_ajax_nextpost.not_in,
		current_user: csco_ajax_nextpost.current_user,
		nonce: csco_ajax_nextpost.nonce,
		next_post: csco_ajax_nextpost.next_post,
	};

	// Request Url.
	var csco_ajax_nextpost_url;
	if ( 'ajax_restapi' === csco_ajax_nextpost.type ) {
		csco_ajax_nextpost_url = csco_ajax_nextpost.rest_url;
	} else {
		csco_ajax_nextpost_url = csco_ajax_nextpost.url;
	}

	// Send Request.
	$.post( csco_ajax_nextpost_url, data, function( res ) {

		csco_ajax_nextpost.next_post = false;

		if ( res.success ) {

			// Get the posts.
			var data = $( res.data.content );

			// Check if there're any posts.
			if ( data.length ) {
				// Set the loading state.
				loadingNextpost = false;

				// Set not_in.
				csco_ajax_nextpost.not_in = res.data.not_in;

				// Set next data.
				csco_ajax_nextpost.next_post = res.data.next_post;

				// Remove loader.
				$( objNextpost ).siblings( '.cs-nextpost-loading' ).remove();

				// Append new post.
				$( objNextpost ).append( data );

				// Reinit facebook.
				if ( $( '#fb-root' ).length ) {
					FB.XFBML.parse();
				}

				$( document.body ).trigger( 'post-load' );
			}
		} else {
			// console.log(res);
		}
	} ).fail( function( xhr, textStatus, e ) {
		// console.log(xhr.responseText);
	} );
}

/**
 * Check if Load Nextpost is defined by the wp_localize_script
 */
if ( typeof csco_ajax_nextpost !== 'undefined' ) {

	// On Scroll Event.
	$( window ).scroll( function() {
		var scrollTop = $( window ).scrollTop();

		// Init nextpost.
		if ( csco_ajax_nextpost.next_post ) {

			if ( objNextpost.length && !loadingNextpost && scrollNextpost.allow ) {
				scrollNextpost.allow = false;
				setTimeout( scrollNextpost.reallow, scrollNextpost.delay );
				// Calc current offset.
				let offset = objNextpost.offset().top + objNextpost.innerHeight() - scrollTop;
				// Load nextpost.
				if ( 4000 > offset ) {
					$( objNextpost ).after( '<div class="cs-nextpost-loading"></div>' );

					csco_ajax_get_nextpost();
				}
			}
		}

		// Reset browser data link.
		let objFirst = $( objNextsect ).first();

		if ( objFirst.length ) {
			let firstTop = $( objFirst ).offset().top;
			// If there has been a change.
			if ( scrollTop < firstTop && window.location.href !== currentNLink ) {
				document.title = currentNTitle;
				window.history.pushState( null, currentNTitle, currentNLink );
			}
		}

		// Set browser data link.
		$( objNextsect ).each( function( index, elem ) {

			let elemTop    = $( elem ).offset().top;
			let elemHeight = $( elem ).innerHeight();

			if ( scrollTop > elemTop  && scrollTop < elemTop + elemHeight ) {
				// If there has been a change.
				if ( window.location.href !==  $( elem ).data( 'url' ) ) {
					// New title.
					document.title = $( elem ).data( 'title' );
					// New link.
					window.history.pushState( null, $( elem ).data( 'title' ), $( elem ).data( 'url' ) );
					// Google Analytics.
					if ( typeof gtag === 'function' && typeof window.gaData === 'object' ) {

						var trackingId = Object.keys( window.gaData )[0];
						if ( trackingId ) {
							gtag( 'config', trackingId, {
								'page_title'    : $( elem ).data( 'title' ),
								'page_location' : $( elem ).data( 'url' )
							} );

							gtag( 'event', 'page_view', { 'send_to': trackingId } );
						}
					}
				}
			}
		} );
	} );
}
