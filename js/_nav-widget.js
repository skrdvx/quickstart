//
// Responsive Navigation
//

$.fn.responsiveNav = function() {
	this.removeClass( 'menu-item-expanded' );
	if ( this.prev().hasClass( 'submenu-visible' ) ) {
		this.prev().removeClass( 'submenu-visible' ).slideUp( 350 );
		this.parent().removeClass( 'menu-item-expanded' );
	} else {
		this.parent().parent().find( '.menu-item .sub-menu' ).removeClass( 'submenu-visible' ).slideUp( 350 );
		this.parent().parent().find( '.menu-item-expanded' ).removeClass( 'menu-item-expanded' );
		this.prev().toggleClass( 'submenu-visible' ).hide().slideToggle( 350 );
		this.parent().toggleClass( 'menu-item-expanded' );
	}
};

//
// Navigation Menu Widget
//

$( document ).ready( function( e ) {

	$( '.widget_nav_menu .menu-item-has-children' ).each( function( e ) {

		// Add a caret.
		$( this ).append( '<span></span>' );

		// Fire responsiveNav() when clicking a caret.
		$( '> span', this ).on( 'click', function( e ) {
			e.preventDefault();
			$( this ).responsiveNav();
		} );

		// Fire responsiveNav() when clicking a parent item with # href attribute.
		if ( '#' === $( '> a', this ).attr( 'href' ) ) {
			$( '> a', this ).on( 'click', function( e ) {
				e.preventDefault();
				$( this ).next().next().responsiveNav();
			} );
		}

	} );

} );
