/*
 * Load Mega Menu Posts
 */
function cscoLoadMenuPosts( menuItem ) {
	var dataCat = menuItem.children( 'a' ).data( 'cat' ),
		dataType = menuItem.children( 'a' ).data( 'type' ),
		dataNumberposts = menuItem.children('a').data( 'numberposts' ),
		menuContainer,
		postsContainer;

	// Containers.
	if ( menuItem.hasClass( 'cs-mega-menu-has-category' ) ) {
		menuContainer  = menuItem;
		postsContainer = menuContainer.find( '.cs-mm-posts' );
	} else {
		menuContainer  = menuItem.closest( '.sub-menu' );
		postsContainer = menuContainer.find( '.cs-mm-posts[data-cat="' + dataCat + '"]' );
	}

	// Set Active.
	menuContainer.find( '.menu-item, .cs-mm-posts' ).removeClass( 'active-item' );
	menuItem.addClass( 'active-item' );

	if ( postsContainer ) {
		postsContainer.addClass( 'active-item' );
	}

	// Check Loading.
	if ( menuItem.hasClass( 'cs-mm-loading' ) || menuItem.hasClass( 'cs-mm-loaded' ) ) {
		return false;
	}

	// Check Category.
	if ( ! dataCat || typeof dataCat === 'undefined' ) {
		return false;
	}

	// Check Container.
	if ( ! postsContainer || typeof postsContainer === 'undefined' ) {
		return false;
	}

	// Create Data.
	var data = {
		'cat'      : dataCat,
		'type'     : dataType,
		'per_page' : dataNumberposts
	};

	// Get Results.
	$.ajax( {
		url: csco_mega_menu.rest_url,
		type: 'GET',
		data: data,
		global: false,
		async: true,
		beforeSend: function() {
			menuItem.addClass( 'cs-mm-loading' );
			postsContainer.addClass( 'cs-mm-loading' );
		},
		success: function( res ) {
			if ( res.status && 'success' === res.status ) {

				// Set the loaded state.
				menuItem.addClass( 'cs-mm-loaded' );
				postsContainer.addClass( 'cs-mm-loaded' );

				// Check if there're any posts.
				if ( res.content && res.content.length ) {

					$( res.content ).imagesLoaded( function() {

						// Append Data.
						postsContainer.html( res.content );
					});
				}
			}
		},
		complete: function() {
			menuItem.removeClass( 'cs-mm-loading' );
			postsContainer.removeClass( 'cs-mm-loading' );
		}
	});
}

/*
 * Get First Tab
 */
function cscoGetFirstTab( container ) {
	var firstTab = false;

	container.find( '.cs-mega-menu-child' ).each( function( index, el ) {
		if ( $( el ).hasClass( 'menu-item-object-category' ) ) {
			firstTab = $( el );
			return false;
		}
		if ( $( el ).hasClass( 'menu-item-object-post_tag' ) ) {
			firstTab = $( el );
			return false;
		}
	} );

	return firstTab;
}

/*
 * Menu on document ready
 */
$( document ).ready( function(){

	/*
	 * Get Menu Posts on Hover
	 */
	$( '.navbar-nav .menu-item.cs-mega-menu-has-category, .navbar-nav .menu-item.cs-mega-menu-child' ).on( 'hover', function() {
		cscoLoadMenuPosts( $( this ) );
	} );

	/*
	 * Load First Tab on Mega Menu Hover
	 */
	$( '.navbar-nav .menu-item.cs-mega-menu-has-categories' ).on( 'hover', function() {
		var tab = cscoGetFirstTab( $( this ) );

		if ( tab ) {
			cscoLoadMenuPosts( tab );
		}
	} );
});

/*
 * Load First Tab on Navbar Ready.
 */
$( document, '.navbar-nav' ).ready( function() {
	var tab = false;

	// Autoload First Tab.
	$( '.navbar-nav .menu-item.cs-mega-menu-has-categories' ).each( function( index, el ) {
		tab = cscoGetFirstTab( $( this ) );
		if ( tab ) {
			cscoLoadMenuPosts( tab );
		}
	} );

	// Autoload Category.
	$( '.navbar-nav .menu-item.cs-mega-menu-has-category' ).each( function( index, el ) {
		cscoLoadMenuPosts( $( this ) );
	} );
} );
