/**
 * AJAX Load More.
 *
 * Contains functions for AJAX Load More.
 */


/**
 * Check if Load More is defined by the wp_localize_script
 */
if ( typeof csco_ajax_pagination !== 'undefined' ) {

	$( '.post-archive' ).append( '<div class="ajax-navigation"><button class="load-more">' + csco_ajax_pagination.translation.load_more + '</button></div>' );

	var query_data = $.parseJSON( csco_ajax_pagination.query_data ),
		infinite = $.parseJSON( query_data.infinite_load ),
		button = $( '.ajax-navigation .load-more' ),
		page = 2,
		loading = false,
		scrollHandling = {
			allow: infinite,
			reallow: function() {
				scrollHandling.allow = true;
			},
			delay: 400 //(milliseconds) adjust to the highest acceptable value
		};

}

/**
 * Get next posts
 */
function csco_ajax_get_posts() {
	loading = true;
	// Set class loading.
	button.addClass( 'loading' );
	var data = {
		action: 'csco_ajax_load_more',
		page: page,
		posts_per_page: csco_ajax_pagination.posts_per_page,
		query_data: csco_ajax_pagination.query_data,
		_ajax_nonce: csco_ajax_pagination.nonce,
	};

	// Request Url.
	var csco_pagination_url;
	if ( 'ajax_restapi' === csco_ajax_pagination.type ) {
		csco_pagination_url = csco_ajax_pagination.rest_url;
	} else {
		csco_pagination_url = csco_ajax_pagination.url;
	}

	// Send Request.
	$.post( csco_pagination_url, data, function( res ) {
		if ( res.success ) {

			// Get the posts.
			var data = $( res.data.content );

			// Check if there're any posts.
			if ( data.length ) {

				var cscoAppendEnd = function() {

					// WP Post Load trigger.
					$( document.body ).trigger( 'post-load' );

					// Reinit Facebook widgets.
					if ( $( '#fb-root' ).length ) {
						FB.XFBML.parse();
					}

					// Remove class loading.
					button.removeClass( 'loading' ).blur();

					// Increment a page.
					page = page + 1;

					// Set the loading state.
					loading = false;
				};

				// Check archive type.
				if ( $( '.post-archive .archive-wrap' ).hasClass( 'archive-type-mixed' ) ) {

					for ( var key in data ) {

						if ( key % 1 !== 0 ) {
							continue;
						}

						var last_section = $( '.archive-wrap .archive-main' ).last();

						var last_posts  = $( last_section ).find('article').length;
						var last_class  = $( last_section ).attr('class');
						var new_section = false;
						var point_end   = 4;

						if ( $( '.site-content' ).hasClass( 'sidebar-disabled' ) ) {
							point_end = 6;
						}

						if ( $( last_section ).hasClass( 'archive-full' ) ) {
							new_section = 'archive-grid';
						}

						if ( $( last_section ).hasClass( 'archive-grid' ) && last_posts === point_end ) {
							new_section = 'archive-full';
						}

						// Append new section.
						if ( new_section ) {
							$( '<div></div>' ).appendTo( '.archive-wrap' )
								.addClass( last_class )
								.removeClass( 'archive-full archive-grid' )
								.addClass( new_section );
						}

						// Append new posts to layout.
						$( '.archive-wrap .archive-main' ).last().append( data[key] );
					}

					cscoAppendEnd();

				} else {
					if ( $( '.post-archive .archive-main' ).hasClass( 'archive-masonry' ) ) {
						data.imagesLoaded( function() {
							// Append new posts to masonry layout.
							$( '.post-archive .archive-main' ).colcade( 'append', data );

							cscoAppendEnd();
						} );
					} else {
						$( '.post-archive .archive-main' ).append( data );

						cscoAppendEnd();
					}
				}
			}

			// Remove Button on Posts End.
			if( res.data.posts_end || ! data.length ) {

				// Remove Load More button.
				$( '.ajax-navigation' ).remove();
			}

		} else {
			// console.log(res);
		}
	} ).fail( function( xhr, textStatus, e ) {
		// console.log(xhr.responseText);
	} );
}

/**
 * Check if Load More is defined by the wp_localize_script
 */
if ( typeof csco_ajax_pagination !== 'undefined' ) {

	// On Scroll Event.
	$( window ).scroll( function() {
		if ( button.length && !loading && scrollHandling.allow ) {
			scrollHandling.allow = false;
			setTimeout( scrollHandling.reallow, scrollHandling.delay );
			var offset = $( button ).offset().top - $( window ).scrollTop();
			if ( 4000 > offset ) {
				csco_ajax_get_posts();
			}
		}
	} );

	// On Click Event.
	$( 'body' ).on( 'click', '.load-more', function() {
		if ( !loading ) {
			csco_ajax_get_posts();
		}
	} );

}
