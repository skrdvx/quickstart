/**
 * Sticky Sidebar
 */

var stickyElements = [];

stickyElements.push( '.sticky-sidebar-enabled.stick-to-top .sidebar-1' );
stickyElements.push( '.sticky-sidebar-enabled.stick-last .sidebar .widget:last-child' );

$( document ).ready( function() {

	// Sticky sidebar for mozilla.
	if ( $.browser.mozilla ) {
		stickyElements.push( '.sticky-sidebar-enabled.stick-to-bottom .sidebar-1' );
	}

	// Join elements.
	stickyElements = stickyElements.join( ',' );

	// Sticky nav visible.
	$( document ).on( 'sticky-nav-visible', function() {
		var navBarHeight = $( '.navbar-primary' ).innerHeight();

		$( stickyElements ).css( 'top', 32 + navBarHeight + 'px' );
	} );

	// Sticky nav hide.
	$( document ).on( 'sticky-nav-hide', function() {
		$( stickyElements ).css( 'top', 32 + 'px' );
	} );

} );