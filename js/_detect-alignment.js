( function() {
	var ticking      = false;

	var update = function() {

		// Sidebar.
		// -----------------------------------.
		$( '.content-area .site-main' ).each(function(){

			var content = $( this ).find( '.entry-content' );
			var sidebar = $( this ).find( '.post-sidebar-inner' );

			// Vars offset.
			var offsetTop    = 20;
			var offsetBottom = -20;

			// Search elements.
			var elements = [];

			elements.push( '> .alignfull' );
			elements.push( '> .alignwide' );

			var layouts = $( content ).find( elements.join(',') );

			if ( 0 === sidebar.length ) {
				return;
			}
			if ( 0 === layouts.length ) {
				return;
			}

			var disabled = false;

			// Get sidebar values.
			var sidebarTop    = $( sidebar ).offset().top;
			var sidebarHeight = $( sidebar ).outerHeight(true);

			for (let i = 0; i < $( layouts ).length; ++i ) {
				if ( 'none' === $( layouts[i] ).css( 'transform' ) ) {
					continue;
				}

				// Get layout values.
				let layoutTop    = $( layouts[i] ).offset().top;
				let layoutHeight = $( layouts[i] ).outerHeight(true);

				// Calc points.
				let pointTop =  layoutTop - offsetTop;
				let pointBottom = layoutTop + layoutHeight + offsetBottom;

				// Detect sidebar location.
				if ( sidebarTop + sidebarHeight >= pointTop && sidebarTop <= pointBottom ) {
					disabled = true;
				}
			}


			if ( disabled ) {
				$( sidebar ).css( 'opacity', '0' );
			} else {
				$( sidebar ).css( 'opacity', '1' );
			}
		});

		// Pagination.
		// -----------------------------------.
		$( '.content-area .site-main' ).each(function(){

			var content = $( this ).find( '.entry-content' );
			var pagination = $( this ).find( '.post-prev-next-along article' );

			// Vars offset.
			var offsetTop    = -20;
			var offsetBottom = -150;

			// Search elements.
			var elements = [];

			elements.push( '> .alignfull' );

			var layouts = $( content ).find( elements.join(',') );

			if ( 0 === pagination.length ) {
				return;
			}
			if ( 0 === layouts.length ) {
				return;
			}

			var disabled = false;

			// Get pagination values.
			var paginationTop    = $( pagination ).offset().top;
			var paginationHeight = $( pagination ).outerHeight(true);

			for (let i = 0; i < $( layouts ).length; ++i ) {
				if ( 'none' === $( layouts[i] ).css( 'transform' ) ) {
					continue;
				}

				// Get layout values.
				let layoutTop    = $( layouts[i] ).offset().top;
				let layoutHeight = $( layouts[i] ).outerHeight(true);

				// Calc points.
				let pointTop =  layoutTop - offsetTop;
				let pointBottom = layoutTop + layoutHeight + offsetBottom;

				// Detect pagination location.
				if ( paginationTop + paginationHeight >= pointTop && paginationTop <= pointBottom ) {
					disabled = true;
				}
			}


			if ( disabled ) {
				$( pagination ).parent().css( 'opacity', '0' );
				$( pagination ).parent().css( 'visibility', 'hidden' );
			} else {
				$( pagination ).parent().css( 'opacity', '1' );
				$( pagination ).parent().css( 'visibility', 'visible' );
			}
		});

		// Ticking.
		ticking = false;
	};

	var requestTick = function() {
		if ( ! ticking ) {
			window.requestAnimationFrame( update );
			ticking = true;
		}
	};

	var onProcess = function() {
		requestTick();
	};

	$( window ).on( 'scroll', onProcess );
	$( window ).on( 'resize', onProcess );
	$( window ).on( 'image-load', onProcess );
	$( window ).on( 'post-load', onProcess );

} )();
