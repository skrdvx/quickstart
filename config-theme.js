/**
 * Theme Configuration.
 *
 * Theme configuration for gulp tasks.
 *
 * In paths you can add <<glob or array of globs>>. Edit the variables as per your theme requirements.
 */

module.exports = {

	// Project related.
	project: 'quickstart', // Project Name.

	// Style related.
	styleSRC: './scss/style.scss', // Path to main .scss file.
	styleDestination: './', // Path to place the compiled CSS file.

	// Customizer style.
	customizerStyleSRC: './scss/customizer.scss', // Path to editor .scss file.
	customizerStyleDestination: './css/', // Path to place the compiled CSS file.

	// Editor style related.
	editorStyleSRC: './scss/editor-style.scss', // Path to editor .scss file.
	editorStyleDestination: './css/', // Path to place the compiled CSS file.

	// Woocommerce style related.
	woocommerceStyleSRC: './scss/woocommerce.scss', // Path to woocommerce .scss file.
	woocommerceStyleDestination: './css/', // Path to place the compiled CSS file.

	// Scripts related.
	scriptsSRC: './js/_*.js', // Path to JS custom scripts folder.
	scriptsDestination: './js/', // Path to place the compiled JS custom scripts file.

	// Watch files paths.
	styleWatchFiles: './scss/**/*.scss', // Path to all *.scss files inside css folder and inside them.
	JSWatchFiles: './js/_*.js', // Path to all JS files.
	PHPWatchFiles: './**/*.php', // Path to all PHP files.

	// Translation related.
	textDomain: 'quickstart', // Your textdomain here.
	translationFile: 'quickstart.pot', // Name of the transalation file.
	translationDestination: './languages', // Where to save the translation files.
	packageName: 'quickstart', // Package name.
	bugReport: 'https://codesupply.co/contact/', // Where can users report bugs.
	lastTranslator: 'Code Supply Co. <hello@codesupply.co>', // Last translator Email ID.
	team: 'Code Supply Co. <hello@codesupply.co>', // Team's Email ID.

	// Browsers you care about for autoprefixing.
	// https://github.com/ai/browserslist
	browsersList: [
		'last 2 version'
	],

	// Vendor scripts that will be copied to theme /js/ folder.
	vendorJS: [
		'node_modules/object-fit-images/dist/ofi.min.js',
		'node_modules/colcade/colcade.js',
		'node_modules/flickity/dist/flickity.pkgd.min.js',
	],

	// Production related.
	// Array of all theme files
	themeFiles: [
		'**/*.php',
		'**/*.css',
		'**/*.ttf',
		'**/*.woff',
		'**/*.woff2',
		'**/*.pot',
		'**/*.po',
		'**/*.mo',
		'**/*.js',
		'**/*.json',
		'**/*.jpg',
		'**/*.gif',
		'**/*.png',
		'**/*.svg',
		'README.txt',
		'wpml-config.xml',
		'!**/*-dev.css',
		'!vendor/**/*',
		'!node_modules/**/*',
		'!js/_*.js',
		'!gulpfile.js',
		'!package.json',
		'!package-lock.json',
		'!config*.js',
		'!deploy.php',
		'!composer.json',
		'!inc/kirki/docs/**/*',
		'!inc/kirki/tests/**/*',
		'!inc/kirki/example.php',
		'!inc/kirki/Gruntfile.js',
		'!inc/kirki/composer.json',
		'!inc/kirki/package.json',
	]

};
