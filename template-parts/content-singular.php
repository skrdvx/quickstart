<?php
/**
 * Template part singular content
 *
 * @package Quickstart
 */

$archive_layout = get_theme_mod( csco_get_archive_option( 'layout' ), 'masonry' );
$summary_type   = get_theme_mod( csco_get_archive_option( 'summary' ), 'excerpt' );
$readmore       = get_theme_mod( csco_get_archive_option( 'read_more' ), true );
$media_preview  = get_theme_mod( csco_get_archive_option( 'media_preview' ), 'cropped' );

// Set class summary.
if ( 'full' === $archive_layout && 'content' === $summary_type ) {
	$class = 'entry-summary-content';
} else {
	$class = 'entry-summary-excerpt';
}
?>

<article <?php post_class( $class ); ?>>

	<!-- Full Post Layout -->
	<?php if ( ! is_singular() ) : ?>

		<div class="post-wrap">
			<div class="post-inner entry-thumbnail">

				<?php
				if ( has_post_thumbnail() ) {

					$ratio = 'landscape';

					// Image size.
					$image_size = 'csco-medium';

					if ( 'disabled' === csco_get_page_sidebar() ) {
						$image_size = 'csco-large';
					}

					if ( 'uncropped' === $media_preview ) {
						$image_size = sprintf( '%s-uncropped', $image_size );

						$ratio = 'uncropped';
					}
				?>

					<div class="cs-overlay cs-overlay-ratio cs-ratio-<?php echo esc_attr( $ratio ); ?> cs-bg-dark">
						<div class="cs-overlay-background">

							<?php the_post_thumbnail( $image_size ); ?>
							<?php csco_get_video_background( 'archive' ); ?>

						</div>
						<div class="cs-overlay-content">

							<?php if ( 'post' === get_post_type() ) { ?>
								<?php csco_the_post_format_icon(); ?>
							<?php } ?>

							<?php if ( 'post' === get_post_type() ) { ?>
								<?php
								$meta_enabled = csco_get_post_meta( array( 'shares', 'views', 'comments', 'reading_time' ), false, false, true );

								if ( $meta_enabled || $readmore ) {
								?>
									<div class="entry-bottom">
										<?php csco_get_post_meta( array( 'shares', 'views', 'comments', 'reading_time' ), false, true, true ); ?>
										<?php if ( true === $readmore ) { ?>
											<a href="<?php the_permalink(); ?>" class="entry-read-more" >
												<?php echo esc_attr( get_theme_mod( 'misc_label_readmore', esc_html__( 'View Post', 'quickstart' ) ) ); ?>
											</a>
										<?php } ?>
									</div>
								<?php } ?>
							<?php } ?>

						</div>
						<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
					</div>

				<?php } ?>

			</div>
			<div class="post-inner entry-data">

				<?php if ( 'post' === get_post_type() && csco_has_post_meta( 'category', csco_get_archive_option( 'post_meta' ) ) ) { ?>
					<div class="entry-category">
						<?php csco_get_post_meta( array( 'category' ), true, true, true ); ?>
					</div>
				<?php } ?>

				<?php if ( get_the_title() ) { ?>
					<h2 class="entry-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_title(); ?>
						</a>
					</h2>
				<?php } ?>

				<?php if ( get_the_excerpt() ) { ?>
					<div class="entry-excerpt">
						<?php echo wp_kses( get_the_excerpt(), 'post' ); ?>
					</div>
				<?php } ?>

				<?php if ( 'post' === get_post_type() ) { ?>
					<?php csco_post_details( csco_get_archive_option( 'post_meta' ) ); ?>
				<?php } ?>

			</div>
		</div>

	<?php endif; ?>

	<?php if ( is_singular() || ( ! is_singular() && ( 'full' === $archive_layout && 'content' === $summary_type ) ) ) { ?>

		<?php do_action( 'csco_singular_content_before' ); ?>

		<!-- Full Post Layout and Full Content -->
		<div class="entry entry-content-wrap">

			<?php do_action( 'csco_singular_content_start' ); ?>

			<div class="entry-content">

				<?php
				$more_link_text = false;

				if ( $readmore ) {
					$more_link_text = sprintf(
						/* translators: %s: Name of current post */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'quickstart' ),
						get_the_title()
					);
				}

				the_content( $more_link_text );
				?>

			</div>
			<?php do_action( 'csco_singular_content_end' ); ?>
		</div>

		<?php do_action( 'csco_singular_content_after' ); ?>

	<?php } ?>

</article>
