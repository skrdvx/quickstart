<?php
/**
 * The template part for displaying post header section.
 *
 * @package Quickstart
 */

$page_header = csco_get_page_header_type();

$class = sprintf( 'entry-header-%s', $page_header );

// Check if post has an image attached.
if ( has_post_thumbnail() ) {
	$class .= ' entry-header-thumbnail';
}
?>

<section class="entry-header <?php echo esc_attr( $class ); ?>">

	<?php
	if ( ! csco_doing_request() ) {
		if ( in_array( $page_header, array( 'standard', 'title', 'small' ), true ) ) {
			csco_breadcrumbs( true );
		}

		if ( in_array( $page_header, array( 'large' ), true ) && ! has_post_thumbnail() ) {
			csco_breadcrumbs( true );
		}
	}
	?>

	<div class="entry-header-inner">

		<?php csco_post_media(); ?>

		<div class="post-header-inner">

			<?php do_action( 'csco_singular_entry_header_start' ); ?>

			<?php if ( is_singular( 'post' ) ) { ?>
				<div class="entry-inline-meta">
					<?php csco_get_post_meta( 'category', false, true, 'post_meta' ); ?>
				</div>
			<?php } ?>

			<?php if ( get_the_title() ) { ?>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php } ?>

			<?php
			if ( 'page' === get_post_type() ) {
				$excerpt_enabled = get_theme_mod( 'page_excerpt', true ) && has_excerpt();
			} else {
				$excerpt_enabled = get_theme_mod( 'post_excerpt', true ) && has_excerpt();
			}

			if ( $excerpt_enabled ) {
				?>
				<div class="post-excerpt"><?php the_excerpt(); ?></div>
				<?php
			}
			?>

			<?php
			if ( is_singular( 'post' ) && csco_get_post_meta( array( 'author', 'date', 'views', 'shares', 'comments', 'reading_time' ), false, false, true ) ) {
				?>
				<div class="entry-post-meta">
					<?php
					csco_post_details( 'post_meta', false, false, esc_html__( 'Written by', 'quickstart' ) );

					csco_get_post_meta( array( 'views', 'shares', 'comments', 'reading_time' ), false, true, 'post_meta' );
					?>
				</div>
				<?php
			}
			?>

			<?php do_action( 'csco_singular_entry_header_end' ); ?>

		</div>

	</div>

</section>
