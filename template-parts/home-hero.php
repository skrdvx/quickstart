<?php
/**
 * Template part for displaying hero section.
 *
 * @package Quickstart
 */

$layout  = get_theme_mod( 'hero_layout', 'left' );
$heading = get_theme_mod( 'hero_title', esc_html__( 'Stay Cool Italian Style', 'quickstart' ) );
$text    = get_theme_mod( 'hero_text', esc_html__( 'Aenean eleifend ante maecenas pulvinar montes lorem et pede dis dolor pretium donec dictum. Vici consequat justo enim.', 'quickstart' ) );
$content = get_theme_mod( 'hero_subscribe', true );
$name    = get_theme_mod( 'hero_subscription_name', false );
$socials = get_theme_mod( 'hero_social_links', false );
$scheme  = get_theme_mod( 'hero_social_links_scheme', 'light' );
$maximum = get_theme_mod( 'hero_social_links_maximum', 3 );
$counts  = get_theme_mod( 'hero_social_links_counts', true );
$image   = get_theme_mod( 'hero_image', '' );
$mask    = get_theme_mod( 'hero_image_mask', 'circle' );

$mask_url = get_stylesheet_directory_uri() . '/css/masks/' . $mask . '/' . $layout . '.svg';

$class_layout = sprintf( 'cs-hero-layout-%s', $layout );

do_action( 'csco_home_hero_before' );
?>

	<div class="section-home-hero">

		<?php do_action( 'csco_home_hero_start' ); ?>

			<div class="cs-container">

					<div class="cs-hero <?php echo esc_attr( $class_layout ); ?>">

						<?php if ( $image ) { ?>
						<div class="cs-hero-image">
							<div class="cs-hero-mask">
								<img src="<?php echo esc_url( $mask_url ); ?>">
							</div>
							<div class="cs-hero-bg">
								<?php echo wp_get_attachment_image( $image, 'csco-large' ); ?>
							</div>
						</div>
						<?php } ?>

						<div class="cs-hero-content">

							<?php if ( $heading ) { ?>
								<h2 class="cs-hero-title">
									<?php echo esc_attr( $heading ); ?>
								</h2>
							<?php } ?>

							<?php if ( $text ) { ?>
								<div class="cs-hero-text">
									<?php echo do_shortcode( $text ); ?>
								</div>
							<?php } ?>

							<?php if ( true === $content && csco_powerkit_module_enabled( 'opt_in_forms' ) ) { ?>
								<div class="cs-hero-subscribe">
									<?php echo do_shortcode( sprintf( '[powerkit_subscription_form display_name="%s"]', $name ) ); ?>
								</div>
							<?php } ?>

							<?php if ( true === $socials && csco_powerkit_module_enabled( 'social_links' ) ) { ?>
								<div class="cs-hero-socials">
									<div class="cs-hero-socials-title">Connect with us</div>
									<?php powerkit_social_links( false, false, $counts, 'nav', $scheme, 'mixed', $maximum ); ?>
								</div>
							<?php } ?>

						</div>

					</div>

			</div>

		<?php do_action( 'csco_home_hero_end' ); ?>

	</div>

<?php do_action( 'csco_home_hero_after' ); ?>
