<?php
/**
 * The template part for displaying default header layout.
 *
 * @package Quickstart
 */

?>

<?php
$layout = get_theme_mod( 'header_layout', 'compact' );

$class = get_theme_mod( 'header_shadow_submenus', false ) ? 'navbar-shadow-enabled' : null;

$color_topbar_bg  = strtoupper( get_theme_mod( 'color_large_header_bg', '#FFFFFF' ) );
$color_content_bg = strtoupper( get_theme_mod( 'color_navbar_bg', '#FFFFFF' ) );

$scheme_topbar  = csco_light_or_dark( $color_topbar_bg, null, ' cs-bg-navbar-dark' );
$scheme_content = csco_light_or_dark( $color_content_bg, null, ' cs-bg-navbar-dark' );

// If the background color in the bars is different.
if ( $color_topbar_bg !== $color_content_bg ) {
	$scheme_topbar .= ' navbar-multicolor';
}
?>

<?php if ( 'with-top-bar' === $layout ) { ?>
	<div class="navbar navbar-topbar <?php echo esc_attr( $class ); ?>">

		<div class="navbar-wrap <?php echo esc_attr( $scheme_topbar ); ?>">

			<div class="navbar-container">

				<div class="navbar-content">

					<div class="navbar-col">
						<?php do_action( 'csco_navbar_content_top_left' ); ?>
					</div>

					<div class="navbar-col">
						<?php do_action( 'csco_navbar_content_top_center' ); ?>
					</div>

					<div class="navbar-col">
						<?php do_action( 'csco_navbar_content_top_right' ); ?>
					</div>
				</div>

			</div>

		</div>

	</div>
<?php } ?>

<nav class="navbar navbar-primary <?php echo esc_attr( $class ); ?>">

	<?php do_action( 'csco_navbar_start' ); ?>

	<div class="navbar-wrap <?php echo esc_attr( $scheme_content ); ?>">

		<div class="navbar-container">

			<div class="navbar-content">

				<div class="navbar-col">
					<?php do_action( 'csco_navbar_content_left' ); ?>
				</div>

				<div class="navbar-col">
					<?php do_action( 'csco_navbar_content_center' ); ?>
				</div>

				<div class="navbar-col">
					<?php do_action( 'csco_navbar_content_right' ); ?>
				</div>

			</div><!-- .navbar-content -->

		</div><!-- .navbar-container -->

	</div><!-- .navbar-wrap -->

	<?php do_action( 'csco_navbar_end' ); ?>

</nav><!-- .navbar -->
