<?php
/**
 * The template part for displaying post prev next section.
 *
 * @package Quickstart
 */

global $post;

$prev_post = get_previous_post();
$next_post = get_next_post();

$class = sprintf( 'post-prev-next-%s', get_theme_mod( 'post_prev_next', 'along' ) );

$class .= ( $prev_post && $next_post ) ? ' post-prev-next-grid' : ' post-prev-next-list';

if ( $prev_post || $next_post ) {

	$image_size = 'csco-thumbnail';

	if ( 'disabled' === csco_get_page_sidebar() ) {
		$image_size = 'csco-medium';
	}
?>
	<div class="post-prev-next <?php echo esc_attr( $class ); ?>">
		<?php
		// Prev post.
		if ( $prev_post ) {
			$post = $prev_post;

			setup_postdata( $post );
			?>
				<div class="link-content prev-link">
					<div class="link-label">
						<a class="link-arrow" href="<?php the_permalink(); ?>">
							<?php esc_html_e( 'Previous Arcticle', 'quickstart' ); ?>
						</a>
					</div>

					<article <?php post_class(); ?>>
						<div class="post-outer">
							<?php
							// Post Thumbnail.
							if ( has_post_thumbnail() ) {
							?>
								<div class="post-inner entry-thumbnail">
									<div class="cs-overlay cs-overlay-transparent cs-overlay-ratio cs-ratio-landscape">
										<div class="cs-overlay-background">
											<?php the_post_thumbnail( $image_size ); ?>
											<?php csco_get_video_background( 'archive' ); ?>
										</div>
										<?php if ( get_post_format() && 'post' === get_post_type() ) { ?>
											<div class="cs-overlay-content">
												<?php csco_the_post_format_icon(); ?>
											</div>
										<?php } ?>
										<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
									</div>
								</div>
							<?php } ?>

							<div class="post-inner">

								<?php csco_post_details( 'archive_post_meta' ); ?>

								<?php if ( get_the_title() ) { ?>
									<header class="entry-header">
										<h2 class="entry-title">
											<?php
											if ( 'post' === get_post_type() && csco_has_post_meta( 'category', 'archive_post_meta' ) ) {
												csco_get_post_meta( array( 'category' ), true, true, 'archive_post_meta' );
											}
											?>
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
												<?php the_title(); ?>
											</a>
										</h2>
									</header>
								<?php } ?>
							</div>

						</div>

					</article>
				</div>
			<?php
			wp_reset_postdata();
		}

		// Next post.
		if ( $next_post ) {
			$post = $next_post;

			setup_postdata( $post );
			?>
				<div class="link-content next-link">
					<div class="link-label">
						<a class="link-arrow" href="<?php the_permalink(); ?>">
							<?php esc_html_e( 'Next Arcticle', 'quickstart' ); ?>
						</a>
					</div>

					<article <?php post_class(); ?>>
						<div class="post-outer">
							<?php
							// Post Thumbnail.
							if ( has_post_thumbnail() ) {
							?>
								<div class="post-inner entry-thumbnail">
									<div class="cs-overlay cs-overlay-transparent cs-overlay-ratio cs-ratio-landscape">
										<div class="cs-overlay-background">
											<?php the_post_thumbnail( $image_size ); ?>
											<?php csco_get_video_background( 'archive' ); ?>
										</div>
										<?php if ( get_post_format() && 'post' === get_post_type() ) { ?>
											<div class="cs-overlay-content">
												<?php csco_the_post_format_icon(); ?>
											</div>
										<?php } ?>
										<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
									</div>
								</div>
							<?php } ?>

							<div class="post-inner">

								<?php csco_post_details( 'archive_post_meta' ); ?>

								<?php if ( get_the_title() ) { ?>
									<header class="entry-header">
										<h2 class="entry-title">
											<?php
											if ( 'post' === get_post_type() && csco_has_post_meta( 'category', 'archive_post_meta' ) ) {
												csco_get_post_meta( array( 'category' ), true, true, 'archive_post_meta' );
											}
											?>
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
												<?php the_title(); ?>
											</a>
										</h2>
									</header>
								<?php } ?>
							</div>

						</div>

					</article>
				</div>
			<?php
		}
		?>
	</div>
<?php
}
