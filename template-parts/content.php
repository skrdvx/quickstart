<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Quickstart
 */

global $wp_query;

// Var Archive type.
if ( get_query_var( 'csco_archive_settings' ) ) {
	$settings = get_query_var( 'csco_archive_settings' );

	$post_meta      = $settings['post_meta'];
	$archive_layout = $settings['layout'];
	$readmore       = $settings['read_more'];
} else {
	$post_meta = csco_get_archive_option( 'post_meta' );

	$archive_layout = get_theme_mod( csco_get_archive_option( 'layout' ), 'masonry' );
	$readmore       = get_theme_mod( csco_get_archive_option( 'read_more' ), true );
}

// Set image size and ratio.
$ratio = 'landscape';

$image_size = 'csco-thumbnail';

if ( 'masonry' === $archive_layout ) {
	$image_size = 'csco-thumbnail-uncropped';

	$ratio = 'uncropped';
}

if ( 'list' === $archive_layout && 'disabled' === csco_get_page_sidebar() ) {
	$image_size = 'csco-medium';
}
?>

<article <?php post_class(); ?>>
	<div class="post-wrap">

		<?php if ( has_post_thumbnail() ) { ?>
			<div class="post-inner entry-thumbnail">

				<div class="cs-overlay cs-overlay-ratio cs-ratio-<?php echo esc_attr( $ratio ); ?> cs-bg-dark">

					<div class="cs-overlay-background">
						<?php the_post_thumbnail( $image_size ); ?>

						<?php
						if ( 'original' !== $ratio ) {
							csco_get_video_background( 'archive' );
						}
						?>
					</div>

					<div class="cs-overlay-content">
						<?php if ( 'post' === get_post_type() ) { ?>
							<?php csco_the_post_format_icon(); ?>
						<?php } ?>

						<?php if ( 'post' === get_post_type() ) { ?>
							<?php
							$meta_enabled = csco_get_post_meta( array( 'shares', 'views', 'comments', 'reading_time' ), false, false, $post_meta );

							if ( $meta_enabled || $readmore ) {
							?>
								<div class="entry-bottom">
									<?php csco_get_post_meta( array( 'shares', 'views', 'comments', 'reading_time' ), true, true, $post_meta ); ?>
									<?php if ( true === $readmore ) { ?>
										<a href="<?php the_permalink(); ?>" class="entry-read-more" >
											<?php echo esc_attr( get_theme_mod( 'misc_label_readmore', esc_html__( 'View Post', 'quickstart' ) ) ); ?>
										</a>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>

					<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
				</div>

			</div>
		<?php } ?>

		<div class="post-inner entry-data">

			<?php if ( 'post' === get_post_type() && csco_has_post_meta( 'category', $post_meta ) ) { ?>
				<div class="entry-category">
					<?php csco_get_post_meta( array( 'category' ), true, true, $post_meta ); ?>
				</div>
			<?php } ?>

			<?php if ( get_the_title() ) { ?>
				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php the_title(); ?>
					</a>
				</h2>
			<?php } ?>

			<?php
			if ( 'original' === $ratio ) {
				csco_get_post_meta( array( 'shares', 'views', 'comments', 'reading_time' ), false, true, $post_meta );
			}
			?>

			<?php if ( get_the_excerpt() ) { ?>
				<div class="entry-excerpt">
					<?php echo wp_kses( get_the_excerpt(), 'post' ); ?>
				</div>
			<?php } ?>

			<?php if ( 'post' === get_post_type() ) { ?>
				<?php csco_post_details( $post_meta ); ?>
			<?php } ?>

		</div>
	</div>
</article>
