<?php
/**
 * Template part for displaying carousel section.
 *
 * @package Quickstart
 */

$ids = csco_get_carousel_ids();

if ( $ids ) {
	$args = array(
		'ignore_sticky_posts' => true,
		'post__in'            => $ids,
		'posts_per_page'      => count( $ids ),
		'post_type'           => array( 'post', 'page' ),
		'orderby'             => 'post__in',
	);

	$the_query = new WP_Query( $args );
}

$carousel_title = get_theme_mod( 'carousel_title', esc_html__( '[[Social Links]] Latest Posts', 'quickstart' ) );

$meta_enabled   = csco_get_post_meta( array( 'views', 'shares', 'comments', 'reading_time' ), true, false, 'carousel_meta' );

$no_meta = '';

if ( ! $meta_enabled ) {
	$no_meta = 'entry-without-meta';
}

// Determines whether there are more posts available in the loop.
if ( $ids && $the_query->have_posts() ) {
	do_action( 'csco_post_carousel_before' );
	?>

	<div class="section-post-carousel">
		<div class="cs-container">

			<?php do_action( 'csco_post_carousel_start' ); ?>

				<?php if ( $carousel_title ) { ?>
					<h3 class="cs-post-carousel-title title-block">
						<?php echo wp_kses( csco_add_support_title_style( $carousel_title ), 'post' ); ?>
					</h3>
				<?php } ?>

				<div class="cs-post-carousel cs-flickity-init">
					<div class="cs-post-carousel-wrap">
						<div class="cs-post-carousel-items">

							<?php while ( $the_query->have_posts() ) : ?>
								<?php $the_query->the_post(); ?>

								<div class="cs-post-carousel-cell">
									<article <?php post_class( 'post-carousel' ); ?>>
										<div class="post-wrap">
											<div class="post-inner entry-thumbnail">

												<div class="cs-overlay cs-overlay-ratio cs-ratio-square cs-bg-dark">
													<div class="cs-overlay-background">

														<?php if ( has_post_thumbnail() ) { ?>
															<?php the_post_thumbnail( 'csco-thumbnail-square', array( 'class' => 'pk-lazyload-disabled' ) ); ?>
															<?php csco_get_video_background( 'tiles', null, 'default', true, false ); ?>
														<?php } ?>

													</div>
													<div class="cs-overlay-content <?php echo esc_attr( $no_meta ); ?>">

														<?php if ( 'post' === get_post_type() ) { ?>
															<?php csco_post_details( 'carousel_meta' ); ?>
														<?php } ?>

														<div class="entry-data <?php echo esc_attr( $no_meta ); ?>">

															<?php if ( 'post' === get_post_type() && csco_has_post_meta( 'category', 'carousel_meta' ) ) { ?>
																<div class="entry-category">
																	<?php csco_get_post_meta( array( 'category' ), true, true, 'carousel_meta' ); ?>
																</div>
															<?php } ?>

															<?php if ( get_the_title() ) { ?>
																<h2 class="entry-title">
																	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
																		<?php the_title(); ?>
																	</a>
																</h2>
															<?php } ?>

															<?php if ( 'post' === get_post_type() ) { ?>
																<?php if ( $meta_enabled ) { ?>
																	<div class="entry-bottom">
																		<?php csco_get_post_meta( array( 'views', 'shares', 'comments', 'reading_time' ), true, true, 'carousel_meta' ); ?>
																	</div>
																<?php } ?>
															<?php } ?>

														</div>

													</div>
													<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
												</div>

											</div>
										</div>
									</article>
								</div>

							<?php endwhile; ?>
						</div>
					</div>

					<div class="cs-post-carousel-sidebar">
						<div class="cs-post-carousel-arrows">
							<a href="#" class="carousel-arrow carousel-next"></a>
							<a href="#" class="carousel-arrow carousel-previous"></a>
						</div>
					</div>

				</div>

				<?php wp_reset_postdata(); ?>

			<?php do_action( 'csco_post_carousel_end' ); ?>

		</div>
	</div>

	<?php
	do_action( 'csco_post_carousel_after' );
}
