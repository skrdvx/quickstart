<?php
/**
 * Template part for displaying tiles section.
 *
 * @package Quickstart
 */

$ids = csco_get_tiles_ids();

if ( $ids ) {
	$args = array(
		'ignore_sticky_posts' => true,
		'post__in'            => $ids,
		'posts_per_page'      => count( $ids ),
		'post_type'           => array( 'post', 'page' ),
		'orderby'             => 'post__in',
	);

	$the_query = new WP_Query( $args );
}

$tile_1_title = get_theme_mod( 'tile_1_title', esc_html__( '[[Latest]] Trending Posts', 'quickstart' ) );

// Determines whether there are more posts available in the loop.
if ( $ids && $the_query->have_posts() ) {
	do_action( 'csco_post_tiles_before' );

	$tiles_type = get_theme_mod( 'tiles_type', 'type-1' );
	?>

	<div class="section-post-tiles">

		<?php do_action( 'csco_post_tiles_start' ); ?>

			<div class="cs-container">

				<div class="cs-post-tiles cs-post-tiles-<?php echo esc_attr( $tiles_type ); ?>">
					<?php
					set_query_var( 'csco_tile_query', $the_query );
					set_query_var( 'csco_tile_thumb_attr', array(
						'class' => 'pk-lazyload-disabled',
					) );

					if ( 'type-1' === $tiles_type ) {
						?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<div class="cs-post-tiles-wrap">
									<h3 class="title-block">
										<?php echo wp_kses( csco_add_support_title_style( $tile_1_title ), 'post' ); ?>
									</h3>
									<?php
										csco_get_posts_tiles( array(
											'tile-list',
											'tile-list',
											'tile-list',
											'tile-list',
											'tile-list',
										) );
									?>
								</div>
							</div>

						<?php } elseif ( 'type-2' === $tiles_type ) { ?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>

						<?php } elseif ( 'type-3' === $tiles_type ) { ?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>

						<?php } elseif ( 'type-4' === $tiles_type ) { ?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>

						<?php } elseif ( 'type-5' === $tiles_type ) { ?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>

						<?php } elseif ( 'type-6' === $tiles_type ) { ?>

							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-full',
									) );
								?>
							</div>
							<div class="cs-post-tiles-column">
								<?php
									csco_get_posts_tiles( array(
										'tile-grid',
										'tile-grid',
									) );
								?>
							</div>

						<?php } ?>
				</div>

				<?php wp_reset_postdata(); ?>
			</div>

		<?php do_action( 'csco_post_tiles_end' ); ?>

	</div>

	<?php
	do_action( 'csco_post_tiles_after' );
}
