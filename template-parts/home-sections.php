<?php
/**
 * Homepage Sections.
 *
 * @package Quickstart
 */

// Get available homepage components.
$components = get_theme_mod( 'homepage_components', csco_homepage_components_default() );

if ( $components && is_array( $components ) ) {

	// Loop through the components.
	foreach ( $components as $component ) {

		// Get Home Hero component template part.
		if ( 'hero' === $component ) {
			csco_homepage_hero();
		}

		// Get Post Tiles component template part.
		if ( 'tiles' === $component ) {
			csco_homepage_tiles();
		}

		// Get Post Carousel component template part.
		if ( 'carousel' === $component ) {
			csco_homepage_carousel();
		}
	}
}
