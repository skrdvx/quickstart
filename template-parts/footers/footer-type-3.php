<?php
/**
 * These functions are used to load template parts (partials) or actions when used within action hooks,
 * and they probably should never be updated or modified.
 *
 * @package Quickstart
 */

?>

<div class="footer-col">
	<?php
	csco_footer_logo();
	csco_footer_social_links();
	csco_footer_description();
	?>
</div>

<div class="footer-col">
	<?php csco_footer_nav_menu(); ?>
</div>

<div class="footer-col">
	<?php csco_footer_subscription(); ?>
</div>
