<?php
/**
 * Template part for content of tile
 *
 * @package Quickstart
 */

global $wp_query;

$query = get_query_var( 'csco_tile_query' );

$query->the_post();

$layout         = get_query_var( 'csco_tile_layout' );
$thumbnail_attr = get_query_var( 'csco_tile_thumb_attr' );

$tiles_type = get_theme_mod( 'tiles_type', 'type-1' );
$readmore   = get_theme_mod( 'home_tiles_read_more', true );

$meta_full_enabled = csco_get_post_meta( array( 'shares', 'views', 'reading_time', 'comments' ), false, false, 'tiles_full_meta' );
$meta_grid_enabled = csco_get_post_meta( array( 'shares', 'views', 'reading_time', 'comments' ), true, false, 'tiles_simple_meta' );

$image_size = 'csco-thumbnail-square';
$ratio      = 'cs-ratio-square';

if ( 'type-1' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-medium-alternative';
		$ratio      = 'cs-ratio-wide';
	}
	if ( 'tile-list' === $layout ) {
		$image_size = 'csco-small';
	}
} elseif ( 'type-2' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-medium-square';
	}
	if ( 'tile-grid' === $layout ) {
		$image_size = 'csco-thumbnail-square';
	}
} elseif ( 'type-3' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-medium-square';
	}
	if ( 'tile-grid' === $layout ) {
		$image_size = 'csco-thumbnail-square';
	}
} elseif ( 'type-4' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-large-square';
	}
	if ( 'tile-grid' === $layout ) {
		$image_size = 'csco-thumbnail-alternative';
	}
} elseif ( 'type-5' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-medium-square';
	}
	if ( 'tile-grid' === $layout ) {
		$image_size = 'csco-thumbnail-square';
	}
} elseif ( 'type-6' === $tiles_type ) {
	if ( 'tile-full' === $layout ) {
		$image_size = 'csco-large-square';
	}
	if ( 'tile-grid' === $layout ) {
		$image_size = 'csco-thumbnail-alternative';
	}
}

$no_grid_meta = '';
$no_full_meta = '';

if ( ! $meta_full_enabled ) {
	$no_full_meta = 'entry-without-meta';
}

if ( ! $meta_grid_enabled ) {
	$no_grid_meta = 'entry-without-meta';
}

$class = sprintf( 'post-tile post-%s', $layout );
?>

<article <?php post_class( $class ); ?>>
	<div class="post-wrap">

		<?php if ( 'tile-full' === $layout ) : ?>

			<div class="post-inner entry-thumbnail">

				<div class="cs-overlay cs-overlay-ratio <?php echo esc_attr( $ratio ); ?> cs-bg-dark">
					<div class="cs-overlay-background">

						<?php if ( has_post_thumbnail() ) { ?>
							<?php the_post_thumbnail( $image_size, (array) $thumbnail_attr ); ?>
							<?php csco_get_video_background( 'tiles' ); ?>
						<?php } ?>

					</div>
					<div class="cs-overlay-content">

						<?php if ( 'post' === get_post_type() ) { ?>
							<?php csco_post_details( 'tiles_full_meta' ); ?>
						<?php } ?>

						<div class="entry-data <?php echo esc_attr( $no_full_meta ); ?>">

							<?php if ( 'post' === get_post_type() && csco_has_post_meta( 'category', 'tiles_post_meta' ) ) { ?>
								<div class="entry-category">
									<?php csco_get_post_meta( array( 'category' ), true, true, 'tiles_full_meta' ); ?>
								</div>
							<?php } ?>

							<?php if ( get_the_title() ) { ?>
								<h2 class="entry-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
									</a>
								</h2>
							<?php } ?>

							<?php if ( get_the_excerpt() ) { ?>
								<div class="entry-excerpt">
									<p><?php echo esc_html( csco_str_truncate( get_the_excerpt(), 100 ) ); ?></p>
								</div>
							<?php } ?>

							<?php if ( 'post' === get_post_type() ) { ?>
								<?php if ( $meta_full_enabled || $readmore ) { ?>
									<div class="entry-bottom">
										<?php csco_get_post_meta( array( 'shares', 'views', 'reading_time', 'comments' ), false, true, 'tiles_full_meta' ); ?>
										<?php if ( true === $readmore ) { ?>
											<a href="<?php the_permalink(); ?>" class="entry-read-more" >
												<?php echo esc_attr( get_theme_mod( 'misc_label_readmore', esc_html__( 'View Post', 'quickstart' ) ) ); ?>
											</a>
										<?php } ?>
									</div>
								<?php } ?>
							<?php } ?>

						</div>

					</div>
					<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
				</div>

			</div>

		<?php elseif ( 'tile-grid' === $layout ) : ?>

			<div class="post-inner entry-thumbnail">

				<div class="cs-overlay cs-overlay-ratio <?php echo esc_attr( $ratio ); ?> cs-bg-dark">
					<div class="cs-overlay-background">

						<?php if ( has_post_thumbnail() ) { ?>
							<?php the_post_thumbnail( $image_size, (array) $thumbnail_attr ); ?>
							<?php csco_get_video_background( 'tiles' ); ?>
						<?php } ?>

					</div>
					<div class="cs-overlay-content">

						<?php if ( 'post' === get_post_type() ) { ?>
							<?php csco_post_details( 'tiles_simple_meta' ); ?>
						<?php } ?>

						<div class="entry-data <?php echo esc_attr( $no_grid_meta ); ?>">

							<?php if ( 'post' === get_post_type() && csco_has_post_meta( 'category', 'tiles_post_meta' ) ) { ?>
								<div class="entry-category">
									<?php csco_get_post_meta( array( 'category' ), true, true, 'tiles_simple_meta' ); ?>
								</div>
							<?php } ?>

							<?php if ( get_the_title() ) { ?>
								<h2 class="entry-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
									</a>
								</h2>
							<?php } ?>

							<?php if ( 'post' === get_post_type() ) { ?>
								<?php if ( $meta_grid_enabled ) { ?>
									<div class="entry-bottom">
										<?php csco_get_post_meta( array( 'shares', 'views', 'reading_time', 'comments' ), true, true, 'tiles_simple_meta' ); ?>
									</div>
								<?php } ?>
							<?php } ?>

						</div>

					</div>
					<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
				</div>

			</div>

		<?php elseif ( 'tile-list' === $layout ) : ?>

			<div class="post-inner">

				<?php if ( has_post_thumbnail() ) { ?>
				<div class="entry-thumbnail">

					<div class="cs-overlay cs-overlay-ratio <?php echo esc_attr( $ratio ); ?>">
						<div class="cs-overlay-background">
							<?php the_post_thumbnail( $image_size, (array) $thumbnail_attr ); ?>
							<?php csco_get_video_background( 'tiles' ); ?>
						</div>
						<a href="<?php the_permalink(); ?>" class="cs-overlay-link"></a>
					</div>

				</div>
				<?php } ?>

				<div class="entry-data">

					<?php if ( get_the_title() ) { ?>
						<h2 class="entry-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
					<?php } ?>

					<?php if ( 'post' === get_post_type() ) { ?>
						<?php if ( csco_get_post_meta( array( 'author', 'date', 'category', 'shares', 'views', 'reading_time', 'comments' ), 'auto', false, 'tiles_simple_meta' ) ) { ?>
							<div class="entry-bottom">
								<?php csco_get_post_meta( array( 'author', 'date', 'category', 'shares', 'views', 'reading_time', 'comments' ), 'auto', true, 'tiles_simple_meta' ); ?>
							</div>
						<?php } ?>
					<?php } ?>

				</div>

			</div>

		<?php endif; ?>

	</div>
</article>
