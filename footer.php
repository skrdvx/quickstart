<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Quickstart
 */

?>

						<?php do_action( 'csco_main_content_end' ); ?>

					</div><!-- .main-content -->

					<?php do_action( 'csco_main_content_after' ); ?>

				</div><!-- .cs-container -->

				<?php do_action( 'csco_site_content_end' ); ?>

			</div><!-- .site-content -->

			<?php do_action( 'csco_site_content_after' ); ?>

			<?php do_action( 'csco_footer_before' ); ?>

			<?php
			// Instagram Timeline.
			$username = get_theme_mod( 'footer_instagram_username' );

			add_filter( 'powerkit_instagram_templates', 'csco_footer_instagram_default', 20 );

			if ( $username && csco_powerkit_module_enabled( 'instagram_integration' ) ) {
				?>
				<div class="footer-instagram">
					<div class="cs-flickity-init">
						<?php
							powerkit_instagram_get_recent( array(
								'user_id' => $username,
								'header'  => get_theme_mod( 'footer_instagram_header', true ),
								'button'  => get_theme_mod( 'footer_instagram_button', true ),
								'number'  => apply_filters( 'csco_instagram_footer_number', 12 ),
								'columns' => apply_filters( 'csco_instagram_footer_columns', 1 ),
								'size'    => 'small',
								'target'  => '_blank',
							) );
						?>
					</div>
				</div>
				<?php
			}

			$function = sprintf( 'remove_%s', 'filter' );

			$function( 'powerkit_instagram_templates', 'csco_footer_instagram_default', 20 );
			?>

			<?php $scheme = csco_light_or_dark( get_theme_mod( 'color_footer_bg', '#FAFAFA' ), null, 'cs-bg-dark' ); ?>

			<footer id="colophon" class="site-footer <?php echo esc_attr( $scheme ); ?>">

				<?php $layout = get_theme_mod( 'footer_layout', 'type-1' ); ?>

				<div class="footer-info footer-layout-<?php echo esc_attr( $layout ); ?>">

					<div class="cs-container">

						<div class="site-info">

							<?php get_template_part( 'template-parts/footers/footer-' . $layout ); ?>

						</div>

					</div>

				</div>

			</footer>

			<?php do_action( 'csco_footer_after' ); ?>

		</div>

	</div><!-- .site-inner -->

	<?php do_action( 'csco_site_end' ); ?>

</div><!-- .site -->

<?php do_action( 'csco_site_after' ); ?>

<?php wp_footer(); ?>
</body>
</html>
