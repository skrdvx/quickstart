<?php
/**
 * Template Name: Meet The Team
 * Template Post Type: page
 *
 * @package Quickstart
 */

esc_html__( 'Meet The Team', 'quickstart' );

// Include default page template.
get_template_part( 'page' );
